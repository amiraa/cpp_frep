# cpp_reps

C++ project to compare search of different design representations against a target static shape, and later against a physical function. 

Search methods used: nelder mead, gradient descent, EM and CMAES.

## Representations

- global Representations
  - [x] parametric frep (~ 5 parameters)
  - [x] frep trees (~ 10 parameters)
  - [x] dft (~ 20 parameters) 
  - [x] DNN/DMN (~ 100 parameters)
  - [x] distance fields (~ 400 parameters) 
  - [ ] point cloud/mesh (~ 1,000 parameters)
  - [x] voxel search (~ 10,000 parameters)
- local representation
  - [ ] morphogenes
  - [ ] Arep
  - [ ] lsystems
- No representation
  - [hybrid cellular automata with local update rules](https://amiraa.pages.cba.mit.edu/cpp_frep/hca.html)


---
## Representation Comparison 

### Comparison Criteria
- Number of Parameters
- Expressiveness
- Convergence rate
- Best Score

### Target Images

All target Images are 100*100

![](img/circle_target.png)
![](img/rectange_target.png)
![](img/gear_target.png)
![](img/blob_target_1.jpg)
![](img/hole_target.png)


----

### Example Best Results

- parametric frep (~ 5 parameters)
- frep trees (~ 10 parameters)
- dft (~ 20 parameters) 
- DNN/DMN (~ 300 parameters)
  - cmaes

![](./img/dnn_cmaes/1.png)
![](./img/dnn_cmaes/2.png)
![](./img/dnn_cmaes/3.png)
![](./img/dnn_cmaes/4.png)
![](./img/dnn_cmaes/5.png)


  - sgd


![](./img/dnn_sgd/1.png)
![](./img/dnn_sgd/2.png)
![](./img/dnn_sgd/3.png)
![](./img/dnn_sgd/4.png)
![](./img/dnn_sgd/5.png)


- distance fields (~ 400 parameters) 

![](./img/df/1.png)
![](./img/df/2.png)
![](./img/df/3.png)
![](./img/df/4.png)
![](./img/df/5.png)


- point cloud/mesh (~ 1,000 parameters)
- voxel search (~ 10,000 parameters)


![](./img/voxel/1.png)
![](./img/voxel/2.png)
![](./img/voxel/3.png)
![](./img/voxel/4.png)
![](./img/voxel/5.png)

---
### Frep tree search strategies

- Constrains and priors
  - constrain circle in the middle
  - symmetry
    - teeth
    - other 
- Search strategies
  - a set graph structure and Parameter search 
  - smooth genome search 
  
<img src="./img/human_in_the_loop_morphing.mp4" width="500" />

<img src="./img/gear_teeth.mp4" width="500" />

<img src="./img/compilation.mp4" width="500" />

- global to local optimization
  - help in constraining number of nodes/complexity
  - MCMC 
    - choose best initial node and parameters as close as possible
    - then choose one that makes better with probability


---
### Local Representation Search

[Link to Project](https://gitlab.cba.mit.edu/amiraa/3d-reaction-diffusion-optimization/-/blob/master/02_Research/MorphogenSearch.md)

- Dynamic systems optimization
- Target: 
  - grow one gear to target shape using gradients
  - grow one gear using hamming distance
  - grow two gears from different points to mesh wih each other


---
### dft progress
![](cpp_dft/dft.gif)

-----

## Dependencies

Similar to [cpp_starter](https://gitlab.cba.mit.edu/erik/cpp_starter/tree/master):
- a modern C++ compiler (i.e. one that supports C++17 features, ex. gcc 7+)
- cmake 3.13 or newer
- Eigen3 (installable via apt, yum, or homebrew)
- openCV (installable via apt, yum, or homebrew)
- json Library: https://github.com/nlohmann/json

## Building

From the project's root directory,

```
mkdir build
cd build
cmake ..
make
```

Tips:
- you can build using multiple threads at once using e.g. `make -j4`
- you can specify a specific compiler using `cmake .. -DCMAKE_CXX_COMPILER=g++-8`
- for debugging symbols use `cmake .. -DCMAKE_BUILD_TYPE=Debug`

## Running

From the build directory,

```
cpp_frep/cpp_frep
cpp_dnn/cpp_dnn
cpp_dft/cpp_dft
cpp_voxel/cpp_voxel
```
