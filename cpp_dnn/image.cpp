#include "nelder_mead.h"
#include "frep.cpp"
#include <iostream>
#include <math.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <armadillo>
#include <iostream>



namespace cpp_dnn {
    using scalar=double;
    using MatrixX = arma::Mat<scalar>;
    using MatrixXI = arma::Mat<long long unsigned int>;

    class Image { 
        public:
            scalar res=0.05;
            scalar bound =2.5;
            scalar minX;
            scalar maxX;
            scalar minY;
            scalar maxY;
            scalar dx;
            scalar dy;
            MatrixX loc;
            MatrixX target;
            int height;
            int width;

            Image():res(0.05),bound(2.5){
                minX=-bound;
                maxX=bound;
                minY=-bound;
                maxY=bound;
                dx=res;
                dy=res;
                setLocMatrix();
            };

            Image(scalar res, scalar bound){
                minX=-bound;
                maxX=bound;
                minY=-bound;
                maxY=bound;
                dx=res;
                dy=res;
                setLocMatrix();
                
            };

            Image(scalar res, scalar boundX,scalar boundY){
                minX=-boundX;
                maxX=boundX;
                minY=-boundY;
                maxY=boundY;
                dx=res;
                dy=res;
                setLocMatrix();
            };

            void setLocMatrix(){
                height=(int)((maxX-minX)/dx);
                width=(int)((maxY-minY)/dy);
                this->loc.zeros(height*width,2);
                unsigned ii=0;
                for(scalar i=minX;i<maxX-dx;i+=dx){
                    for(scalar j=minY;j<maxY-dy;j+=dy){
                        this->loc(ii,0)=i;
                        this->loc(ii,1)=j;
                        ii++;
                    }
                }
            }

            MatrixX getLocMatrix(){
                return this->loc.t();
            }

            MatrixXI predict (const MatrixX& Ypred) {
                return arma::index_max(Ypred,0) ;
            }

            MatrixX getTarget( cpp_frep::Frep* frep,std::string name){
                target.zeros(height*width,2);
                for (unsigned i=0; i<this->loc.n_rows; i++){
                    frep->setXY(this->loc(i,0),this->loc(i,1));
                    if(frep->eval()>0.0){
                        target(i, 0) = 0.0;
                        target(i, 1) = 1.0;
                    }else{
                        target(i, 0) = 1.0;
                        target(i, 1) = 0.0;
                    }
                }
                saveImage(predict(target.t()) ,"Target_"+ name);
                return target.t();
            }
            
            int getHamming(const MatrixXI& Ypred,const MatrixXI& YY){
                int d=0;
                for(unsigned i=0;i<Ypred.size();i++){
                    if((Ypred(i)>0.0 && YY(i)>0.0 )||(Ypred(i)<=0.0 && YY(i)<=0.0)){
                    }else{
                        d++;
                    }
                }
                return d;
            }

            void saveImage(const MatrixXI& Ypred,std::string name){
                const int size=height*width*4;
                int stride=0;
                unsigned char pix[size];

    
                for(unsigned i=0;i<Ypred.size();i++){
                    if(Ypred(i)>0.0){
                        pix[stride+0]=0;
                        pix[stride+1]=0;
                        pix[stride+2]=0;
                        pix[stride+3]=0;
                        
                    }else{
                        pix[stride+0]=255;
                        pix[stride+1]=255;
                        pix[stride+2]=255;
                        pix[stride+3]=255;
                    }
                    stride+=4;
                }

                ////////////////create image and save it//////////////////////////
                cv::Mat image = cv::Mat(width, height, CV_8UC4, pix);
                std::string fileName="img_dnn/"+name+".png";
                cv::imwrite( fileName, image );
                bool preview=false;
                if(preview){
                    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
                    cv::imshow("Display Image", image);
                    cv::waitKey(0);
                }

            }

            virtual ~Image() {
                // std::cout<<"deleted_"<<name<<" \n";
            }
        
    };
}

