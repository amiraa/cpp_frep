#include "dnn.cpp"
#include "Image.cpp"
#include "vector.h"
#include <iostream>
#include <armadillo>

#include <stdio.h>
#include <stdlib.h> /* free() */
#include <stddef.h> /* NULL */

#include "cmaes_interface.h"

using namespace cpp_dnn;
using scalar=double;

scalar getRandom(){//between 0 and 1
    return ((scalar) rand() / (RAND_MAX));
}

std::vector<scalar> getRandomWeights(unsigned numWeights){
    std::vector<scalar> v;
   
    for(unsigned i=0;i<numWeights;i++){
        v.push_back(getRandom()*3.0-6.0);
    }
    return v;
}

/* the objective (fitness) function to be minimized */
// double fitfun(double const *x, int N) { /* function "cigtab" */
//   int i; 
//   double sum = 1e4*x[0]*x[0] + 1e-4*x[1]*x[1];
//   for(i = 2; i < N; ++i)  
//     sum += x[i]*x[i]; 
//   return sum;
// }
std::vector<scalar>  pointerToVector(double const *x, int N){
  std::vector<scalar> v;
  for(int i = 0; i < N; ++i)  
    v.push_back(x[i]);
  return v;
}

// /* the objective (fitness) function to be minimized */
double fitfun(double const *x, int N,MatrixX X,MatrixX Y,Sequential seq) { /* function "cigtab" */
  seq.setWeights( pointerToVector(x, N));
  seq.step(X,Y);
  return seq.sumLoss();
}

//--------------------------------------------------------------------------------------------------
int main(int const, char const**) {

    //Create network
    std::vector<Node*> network;

    Linear l1(2, 12);
    Tanh a1;

    Linear l2(12, 12);
    Tanh a2;

    Linear l3(12, 6);
    Tanh a3;

    Linear l4(6, 6);
    Tanh a4;

    Linear l5(6, 2);
    SoftMax a5;

    NLL l;
    network.push_back( &l1);
    network.push_back( &a1);
    network.push_back( &l2);
    network.push_back( &a2);
    network.push_back( &l3);
    network.push_back( &a3);
    network.push_back( &l4);
    network.push_back( &a4);
    network.push_back( &l5);
    network.push_back( &a5);

    Sequential seq(network, &l);

    //Create Image
    Image Im;
    MatrixX X=Im.getLocMatrix();
    cpp_frep::Circle frep(1.5f,0.0f,0.0f);
    MatrixX Y=Im.getTarget( &frep ,"Circle");
    // Im.saveImage(seq.step(X,Y),"trial"); //test

    ///////////////////////////// //Optimize neldermead
    unsigned numWeights=seq.getWeightsNum();
  
    // std::vector<scalar> v=getRandomWeights(numWeights);
    // std::cout << seq.sumLoss()<< '\n';
    // seq.setWeights(v);
    // Im.saveImage(seq.step(X,Y),"trial1"); //test
    // std::cout << seq.sumLoss()<< '\n';


    // float precision = 0.001;
    // int dimension = (int)numWeights;
    // unsigned maxSteps=1000;
    // NelderMeadOptimizer o(dimension, precision);
    // std::cout << dimension<< '\n';

    // request a simplex to start with
    // Vector v(getRandomWeights(numWeights));
    // Vector v1(getRandomWeights(numWeights));
    // Vector v2(getRandomWeights(numWeights));
    // o.insert(v);
    // o.insert(v1);
    // o.insert(v2);
    // unsigned count=0;

    // while (!o.done() && count<maxSteps) {
    //     seq.setWeights(v.get());
    //     Im.saveImage(seq.step(X,Y),"trial"+std::to_string(count));
    //     v = o.step(v, -seq.sumLoss()); //later change f(v)
    //     count++;

    // }


    ///////////////////////////// //Optimize CMAES
    cmaes_t evo; /* an CMA-ES type struct or "object" */
    double *arFunvals, *const*pop, *xfinal;
    int i; 
    arFunvals = cmaes_init(&evo, 5, NULL, NULL, 0, 0, "cpp_dnn/cmaes_initials.par");
    printf("%s\n", cmaes_SayHello(&evo));
     
    cmaes_ReadSignals(&evo, "cmaes_signals.par");  /* write header and initial values */

    /* Iterate until stop criterion holds */
    while(!cmaes_TestForTermination(&evo))
    { 
      /* generate lambda new search points, sample population */
      pop = cmaes_SamplePopulation(&evo); /* do not change content of pop */

      /* Here we may resample each solution point pop[i] until it
	 	 becomes feasible. function is_feasible(...) needs to be
	 	 user-defined.
	 	 Assumptions: the feasible domain is convex, the optimum is
	 	 not on (or very close to) the domain boundary, initialX is
	 	 feasible and initialStandardDeviations are sufficiently small
	 	 to prevent quasi-infinite looping. */
      /* for (i = 0; i < cmaes_Get(&evo, "popsize"); ++i)
           while (!is_feasible(pop[i]))
             cmaes_ReSampleSingle(&evo, i);
      */

      /* evaluate the new search points using fitfun */
      for (i = 0; i < cmaes_Get(&evo, "lambda"); ++i) {
    	  arFunvals[i] = fitfun(pop[i], (int) cmaes_Get(&evo, "dim"),X,Y,seq);
      }

      /* update the search distribution used for cmaes_SamplePopulation() */
      cmaes_UpdateDistribution(&evo, arFunvals);  

      /* read instructions for printing output or changing termination conditions */ 
      cmaes_ReadSignals(&evo, "cmaes_signals.par");
      fflush(stdout); /* useful in MinGW */
    }
    printf("Stop:\n%s\n",  cmaes_TestForTermination(&evo)); /* print termination reason */
    cmaes_WriteToFile(&evo, "all", "allcmaes.dat");         /* write final results */

    /* get best estimator for the optimum, xmean */
    xfinal = cmaes_GetNew(&evo, "xmean"); /* "xbestever" might be used as well */
    cmaes_exit(&evo); /* release memory */ 

    /* do something with final solution and finally release memory */
    std::cout<<*xfinal<<'\n';
    
    free(xfinal); 

    seq.setWeights( pointerToVector(pop[0], (int) cmaes_Get(&evo, "dim")));
    Im.saveImage(seq.step(X,Y),"trialll");
    
    

  
    return 0;
}
