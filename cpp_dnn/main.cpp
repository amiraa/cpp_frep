#include "dnn.cpp"
#include "Image.cpp"
#include "vector.h"
#include <iostream>
#include <armadillo>

#include <stdio.h>
#include <stdlib.h> /* free() */
#include <stddef.h> /* NULL */

#include "cmaes_interface.h"

using namespace cpp_dnn;
using scalar=double;

int COUNT=0;

scalar getRandom(){//between 0 and 1
    return ((scalar) rand() / (RAND_MAX));
}

std::vector<scalar> getRandomWeights(unsigned numWeights){
    std::vector<scalar> v;
   
    for(unsigned i=0;i<numWeights;i++){
        v.push_back(getRandom()*3.0-6.0);
    }
    return v;
}

/* the objective (fitness) function to be minimized */
double fitfun2(double const *x, int N) { /* function "cigtab" */
  int i; 
  double sum = 1e4*x[0]*x[0] + 1e-4*x[1]*x[1];
  for(i = 2; i < N; ++i)  
    sum += x[i]*x[i]; 
  return sum;
}


std::vector<scalar>  pointerToVector(double const *x, int N){
  std::vector<scalar> v;
  for(int i = 0; i < N; ++i)  
    v.push_back(x[i]);
  return v;
}

// /* the objective (fitness) function to be minimized */
double fitfun(double const *x, int N,MatrixX X,MatrixX Y,Sequential seq) { /* function "cigtab" */
  Image Im;
  MatrixX XX=Im.getLocMatrix();
  seq.setWeights( pointerToVector(x, N));
  auto Ypred=seq.step(X,Y);
  auto YY=Im.predict(Y);
  double res=(double) Im.getHamming(Ypred,YY);
  if(COUNT%100==0){
    Im.saveImage(Ypred,std::to_string(COUNT)); //test
    cout<<"iter:"<<COUNT<<" score:"<<res<<endl;

  }
  COUNT++;
  
  
  return res;
}

//--------------------------------------------------------------------------------------------------
int main(int const, char const**) {

  scalar res=0.05f;
    scalar bound =2.5f;


    
    scalar minX=-bound;
    scalar maxX=bound;
    scalar minY=-bound;
    scalar maxY=bound;
    scalar dx=res;
    scalar dy=res;
    int height=(int)((maxX-minX)/dx);
    int width=(int)((maxY-minY)/dy);

    cout<<"width: "<<width<<", height: "<<height <<std::endl;

    
    //Create Image
    Image Im;
    MatrixX X=Im.getLocMatrix();

    
    
    cpp_frep::Circle c(1.5f,0.0f,0.0f);
    // std::vector<unsigned char> targ1 =drawFrep(&c,minX, maxX, minY, maxY, dx, dy,"circle_target",false);
    MatrixX targ1=Im.getTarget( &c,"circle");

    cpp_frep::Rectangle r(-1.5f,1.5f,-1.0f,1.0f);
    cpp_frep::Rotate rot1 (&r,0.0f,0.0f,90.0f);
    // std::vector<unsigned char> targ2 =drawFrep(&rot1,minX, maxX, minY, maxY, dx, dy,"rectange_target",false);
    MatrixX targ2=Im.getTarget( &rot1,"rectange");

    cpp_frep::InvoluteGear gear( 1.0f,  1.0f, 1.1f, 20.0f, 8.0f) ;
    cpp_frep::Rotate rot (&gear,0.0f,0.0f,15.0f);
    cpp_frep::Scale sc1(&rot,0.0f,0.0f,0.5f,0.5f);
    cpp_frep::Add target(&sc1,&c);
    // std::vector<unsigned char> targ3 =drawFrep(&target,minX, maxX, minY, maxY, dx, dy,"gear_target",false);
    MatrixX targ3=Im.getTarget( &target,"gear");


    cpp_frep::Circle c1(0.9f,-0.5f,0.8f);
    cpp_frep::Circle c2(0.8f,0.2f,-0.2f);
    cpp_frep::Circle c3(0.9f,1.5f,0.0f);
    cpp_frep::Circle c4(0.9f,1.7f,-1.1f);
    // Subtract ss(&rot1,&c1);
    
    // Subtract sss(&ss,&c2);
    cpp_frep::Add aa(&c1,&c3);
    cpp_frep::Add aaa(&aa,&c2);
    cpp_frep::Add a(&aaa,&c4);
    cpp_frep::Morph m (&a,&target,0.6f);
    cpp_frep::Rotate rot2 (&m,0.3f,0.0f,-90.0f);
    // Scale scc (&m,0.0f,0.0f,1.0,1.0);
    // std::vector<unsigned char> targ4 =drawFrep(&rot2,minX, maxX, minY, maxY, dx, dy,"blob_target",false);
    MatrixX targ4=Im.getTarget( &rot2,"blob");

    cpp_frep::Circle c5(0.5f,0.0f,0.0f);
    cpp_frep::Subtract ss(&c,&c5);
    // std::vector<unsigned char> targ5 =drawFrep(&ss,minX, maxX, minY, maxY, dx, dy,"hole_target",false);
    MatrixX targ5=Im.getTarget( &ss,"hole");

    ////////////////////////////////////////////////////

    //Create network
    std::vector<Node*> network;

    Linear l1(2, 12);
    Tanh a1;

    Linear l2(12, 12);
    ReLU a2;

    Linear l3(12, 6);
    ReLU a3;

    Linear l4(6, 6);
    Tanh a4;

    Linear l5(6, 2);
    SoftMax a5;

    NLL l;
    network.push_back( &l1);
    network.push_back( &a1);
    network.push_back( &l2);
    network.push_back( &a2);
    network.push_back( &l3);
    network.push_back( &a3);
    network.push_back( &l4);
    network.push_back( &a4);
    network.push_back( &l5);
    network.push_back( &a5);

    Sequential seq(network, &l);

    ///////TARGET/////
    MatrixX Y=targ5; /////////////////////////////////////////////target
    Im.saveImage(seq.step(X,Y),"trial"); //test

    std::cout<<"Number of Weights:"<<(int)seq.getWeightsNum()<<std::endl;

    // ///////////////////////////// //Optimize neldermead
    // unsigned numWeights=seq.getWeightsNum();
    auto YY=Im.predict(Y);


    ///////SGD/////

    // unsigned epoch=2500;
    // unsigned it=1000;//10000
    // scalar lrate = 0.005;
    // for (unsigned i=0;i<epoch;i++){
    //   if(i%25==0){
    //     auto Ypred=seq.step(X,Y);
    //     Im.saveImage(Ypred,std::to_string(i)); //test
    //     std::cout<<"iter:"<<i<<" score:"<<Im.getHamming(Ypred,YY)<<std::endl;
    //   }
    //   seq.sgd(X, Y, it, lrate);
    // }
    

    
    // Modifies the weights and biases
    

    ////cmaes//
    cmaes_t evo; /* an CMA-ES type struct or "object" */
    double *arFunvals, *const*pop, *xfinal;
    int i; 
    arFunvals = cmaes_init(&evo, 5, NULL, NULL, 0, 0, "cpp_dnn/cmaes_initials.par");
    printf("%s\n", cmaes_SayHello(&evo));
     
    cmaes_ReadSignals(&evo, "cmaes_signals.par");  /* write header and initial values */

    /* Iterate until stop criterion holds */
    while(!cmaes_TestForTermination(&evo))
    { 
      /* generate lambda new search points, sample population */
      pop = cmaes_SamplePopulation(&evo); /* do not change content of pop */

      /* Here we may resample each solution point pop[i] until it
	 	 becomes feasible. function is_feasible(...) needs to be
	 	 user-defined.
	 	 Assumptions: the feasible domain is convex, the optimum is
	 	 not on (or very close to) the domain boundary, initialX is
	 	 feasible and initialStandardDeviations are sufficiently small
	 	 to prevent quasi-infinite looping. */
      /* for (i = 0; i < cmaes_Get(&evo, "popsize"); ++i)
           while (!is_feasible(pop[i]))
             cmaes_ReSampleSingle(&evo, i);
      */

      /* evaluate the new search points using fitfun */
      for (i = 0; i < cmaes_Get(&evo, "lambda"); ++i) {
    	  arFunvals[i] = fitfun(pop[i], (int) cmaes_Get(&evo, "dim"),X,Y,seq);
      }

      /* update the search distribution used for cmaes_SamplePopulation() */
      cmaes_UpdateDistribution(&evo, arFunvals);  

      /* read instructions for printing output or changing termination conditions */ 
      cmaes_ReadSignals(&evo, "cmaes_signals.par");
      fflush(stdout); /* useful in MinGW */
    }
    printf("Stop:\n%s\n",  cmaes_TestForTermination(&evo)); /* print termination reason */
    cmaes_WriteToFile(&evo, "all", "allcmaes.dat");         /* write final results */

    /* get best estimator for the optimum, xmean */
    xfinal = cmaes_GetNew(&evo, "xmean"); /* "xbestever" might be used as well */
    cmaes_exit(&evo); /* release memory */ 

    /* do something with final solution and finally release memory */
    std::cout<<*xfinal<<'\n';
    
    free(xfinal); 

    seq.setWeights( pointerToVector(pop[0], (int) cmaes_Get(&evo, "dim")));
    Im.saveImage(seq.step(X,Y),"trialll");
    




    
  
    return 0;
}

//dnn_sgd target 1
  //   iter:0 score:7266
  // iter:25 score:286
  // iter:50 score:302
  // iter:75 score:182
  // iter:100 score:264
  // iter:125 score:390
  // iter:150 score:83
  // iter:175 score:125
  // iter:200 score:187
  // iter:225 score:189
  // iter:250 score:188
  // iter:275 score:164
  // iter:300 score:151
  // iter:325 score:98
  // iter:350 score:74
  // iter:375 score:118
  // iter:400 score:175
  // iter:425 score:59
  // iter:450 score:103
  // iter:475 score:126
  // iter:500 score:88
  // iter:525 score:79
  // iter:550 score:110
  // iter:575 score:98
  // iter:600 score:126
  // iter:625 score:153
  // iter:650 score:149
  // iter:675 score:76
  // iter:700 score:65
  // iter:725 score:70
  // iter:750 score:76
  // iter:775 score:50
  // iter:800 score:68
  // iter:825 score:89
  // iter:850 score:141
  // iter:875 score:142
  // iter:900 score:154
  // iter:925 score:47
  // iter:950 score:75
  // iter:975 score:108
  // iter:1000 score:53
  // iter:1025 score:69
  // iter:1050 score:120
  // iter:1075 score:62
  // iter:1100 score:70
  // iter:1125 score:59
  // iter:1150 score:122
  // iter:1175 score:107
  // iter:1200 score:84
  // iter:1225 score:77
  // iter:1250 score:67
  // iter:1275 score:86
  // iter:1300 score:76
  // iter:1325 score:139
  // iter:1350 score:43
  // iter:1375 score:119
  // iter:1400 score:113
  // iter:1425 score:68
  // iter:1450 score:63
  // iter:1475 score:44
  // iter:1500 score:163
  // iter:1525 score:50
  // iter:1550 score:118
  // iter:1575 score:65
  // iter:1600 score:72
  // iter:1625 score:93
  // iter:1650 score:33
  // iter:1675 score:44
  // iter:1700 score:52
  // iter:1725 score:107
  // iter:1750 score:67
  // iter:1775 score:34
  // iter:1800 score:66
  // iter:1825 score:81
  // iter:1850 score:47
  // iter:1875 score:43
  // iter:1900 score:63
  // iter:1925 score:45
  // iter:1950 score:43
  // iter:1975 score:79
  // iter:2000 score:53
  // iter:2025 score:63
  // iter:2050 score:52
  // iter:2075 score:83
  // iter:2100 score:62
  // iter:2125 score:90
  // iter:2150 score:61
  // iter:2175 score:95
  // iter:2200 score:77
  // iter:2225 score:79
  // iter:2250 score:85
  // iter:2275 score:57
  // iter:2300 score:89
  // iter:2325 score:36
  // iter:2350 score:56
  // iter:2375 score:30
  // iter:2400 score:54
  // iter:2425 score:140
  // iter:2450 score:52
  // iter:2475 score:69
//dnn_sgd target 2
  //   Number of Weights:326
  // iter:0 score:7760
  // iter:25 score:459
  // iter:50 score:270
  // iter:75 score:344
  // iter:100 score:142
  // iter:125 score:138
  // iter:150 score:203
  // iter:175 score:264
  // iter:200 score:137
  // iter:225 score:98
  // iter:250 score:84
  // iter:275 score:202
  // iter:300 score:134
  // iter:325 score:106
  // iter:350 score:62
  // iter:375 score:88
  // iter:400 score:133
  // iter:425 score:106
  // iter:450 score:91
  // iter:475 score:227
  // iter:500 score:201
  // iter:525 score:141
  // iter:550 score:90
  // iter:575 score:36
  // iter:600 score:105
  // iter:625 score:106
  // iter:650 score:80
  // iter:675 score:67
  // iter:700 score:26
  // iter:725 score:59
  // iter:750 score:69
  // iter:775 score:45
  // iter:800 score:85
  // iter:825 score:97
  // iter:850 score:32
  // iter:875 score:42
  // iter:900 score:109
  // iter:925 score:43
  // iter:950 score:77
  // iter:975 score:59
  // iter:1000 score:73
  // iter:1025 score:68
  // iter:1050 score:101
  // iter:1075 score:105
  // iter:1100 score:46
  // iter:1125 score:19
  // iter:1150 score:96
  // iter:1175 score:36
  // iter:1200 score:56
  // iter:1225 score:35
  // iter:1250 score:32
  // iter:1275 score:64
  // iter:1300 score:70
  // iter:1325 score:33
  // iter:1350 score:20
  // iter:1375 score:43
  // iter:1400 score:35
  // iter:1425 score:19
  // iter:1450 score:33
  // iter:1475 score:27
  // iter:1500 score:18
  // iter:1525 score:47
  // iter:1550 score:16
  // iter:1575 score:19
  // iter:1600 score:6
  // iter:1625 score:32
  // iter:1650 score:16
  // iter:1675 score:61
  // iter:1700 score:22
  // iter:1725 score:8
  // iter:1750 score:15
  // iter:1775 score:23
  // iter:1800 score:10
  // iter:1825 score:6
  // iter:1850 score:29
  // iter:1875 score:21
  // iter:1900 score:10
  // iter:1925 score:9
  // iter:1950 score:4
  // iter:1975 score:5
  // iter:2000 score:23
  // iter:2025 score:67
  // iter:2050 score:111
  // iter:2075 score:34
  // iter:2100 score:6
  // iter:2125 score:29
  // iter:2150 score:18
  // iter:2175 score:48
  // iter:2200 score:5
  // iter:2225 score:12
  // iter:2250 score:10
  // iter:2275 score:24
  // iter:2300 score:14
  // iter:2325 score:1
  // iter:2350 score:8
  // iter:2375 score:2
  // iter:2400 score:17
  // iter:2425 score:20
  // iter:2450 score:3
  // iter:2475 score:5
//dnn_sgd target 3
  // Number of Weights:326
  // iter:0 score:5168
  // iter:25 score:1175
  // iter:50 score:1110
  // iter:75 score:1031
  // iter:100 score:1106
  // iter:125 score:872
  // iter:150 score:989
  // iter:175 score:745
  // iter:200 score:905
  // iter:225 score:825
  // iter:250 score:665
  // iter:275 score:694
  // iter:300 score:903
  // iter:325 score:692
  // iter:350 score:792
  // iter:375 score:767
  // iter:400 score:695
  // iter:425 score:633
  // iter:450 score:699
  // iter:475 score:719
  // iter:500 score:642
  // iter:525 score:704
  // iter:550 score:634
  // iter:575 score:547
  // iter:600 score:581
  // iter:625 score:541
  // iter:650 score:723
  // iter:675 score:610
  // iter:700 score:658
  // iter:725 score:595
  // iter:750 score:595
  // iter:775 score:669
  // iter:800 score:672
  // iter:825 score:723
  // iter:850 score:834
  // iter:875 score:593
  // iter:900 score:609
  // iter:925 score:582
  // iter:950 score:592
  // iter:975 score:511
  // iter:1000 score:579
  // iter:1025 score:537
  // iter:1050 score:536
  // iter:1075 score:512
  // iter:1100 score:606
  // iter:1125 score:571
  // iter:1150 score:532
  // iter:1175 score:607
  // iter:1200 score:606
  // iter:1225 score:496
  // iter:1250 score:492
  // iter:1275 score:583
  // iter:1300 score:621
  // iter:1325 score:521
  // iter:1350 score:672
  // iter:1375 score:643
  // iter:1400 score:607
  // iter:1425 score:551
  // iter:1450 score:540
  // iter:1475 score:573
  // iter:1500 score:622
  // iter:1525 score:499
  // iter:1550 score:510
  // iter:1575 score:584
  // iter:1600 score:542
  // iter:1625 score:541
  // iter:1650 score:573
  // iter:1675 score:607
  // iter:1700 score:546
  // iter:1725 score:661
  // iter:1750 score:548
  // iter:1775 score:533
  // iter:1800 score:695
  // iter:1825 score:592
  // iter:1850 score:553
  // iter:1875 score:557
  // iter:1900 score:590
  // iter:1925 score:515
  // iter:1950 score:559
  // iter:1975 score:514
  // iter:2000 score:490
  // iter:2025 score:539
  // iter:2050 score:591
  // iter:2075 score:517
  // iter:2100 score:547
  // iter:2125 score:560
  // iter:2150 score:554
  // iter:2175 score:468
  // iter:2200 score:547
  // iter:2225 score:559
  // iter:2250 score:551
  // iter:2275 score:651
  // iter:2300 score:582
  // iter:2325 score:617
  // iter:2350 score:606
  // iter:2375 score:550
  // iter:2400 score:506
  // iter:2425 score:837
  // iter:2450 score:534
  // iter:2475 score:566
//dnn_sgd target 4
  //   width: 99, height: 99
  // Number of Weights:326
  // iter:0 score:6479
  // iter:25 score:484
  // iter:50 score:220
  // iter:75 score:244
  // iter:100 score:350
  // iter:125 score:325
  // iter:150 score:374
  // iter:175 score:303
  // iter:200 score:425
  // iter:225 score:184
  // iter:250 score:248
  // iter:275 score:229
  // iter:300 score:197
  // iter:325 score:252
  // iter:350 score:316
  // iter:375 score:222
  // iter:400 score:194
  // iter:425 score:134
  // iter:450 score:201
  // iter:475 score:146
  // iter:500 score:166
  // iter:525 score:187
  // iter:550 score:198
  // iter:575 score:237
  // iter:600 score:185
  // iter:625 score:154
  // iter:650 score:173
  // iter:675 score:364
  // iter:700 score:155
  // iter:725 score:381
  // iter:750 score:165
  // iter:775 score:224
  // iter:800 score:169
  // iter:825 score:157
  // iter:850 score:182
  // iter:875 score:163
  // iter:900 score:154
  // iter:925 score:143
  // iter:950 score:201
  // iter:975 score:291
  // iter:1000 score:198
  // iter:1025 score:161
  // iter:1050 score:131
  // iter:1075 score:188
  // iter:1100 score:185
  // iter:1125 score:132
  // iter:1150 score:144
  // iter:1175 score:217
  // iter:1200 score:210
  // iter:1225 score:160
  // iter:1250 score:183
  // iter:1275 score:202
  // iter:1300 score:257
  // iter:1325 score:210
  // iter:1350 score:163
  // iter:1375 score:118
  // iter:1400 score:197
  // iter:1425 score:160
  // iter:1450 score:158
  // iter:1475 score:166
  // iter:1500 score:154
  // iter:1525 score:162
  // iter:1550 score:211
  // iter:1575 score:150
  // iter:1600 score:164
  // iter:1625 score:193
  // iter:1650 score:123
  // iter:1675 score:146
  // iter:1700 score:105
  // iter:1725 score:185
  // iter:1750 score:234
  // iter:1775 score:101
  // iter:1800 score:136
  // iter:1825 score:122
  // iter:1850 score:115
  // iter:1875 score:199
  // iter:1900 score:121
  // iter:1925 score:130
  // iter:1950 score:180
  // iter:1975 score:85
  // iter:2000 score:154
  // iter:2025 score:154
  // iter:2050 score:155
  // iter:2075 score:161
  // iter:2100 score:169
  // iter:2125 score:132
  // iter:2150 score:165
  // iter:2175 score:126
  // iter:2200 score:123
  // iter:2225 score:115
  // iter:2250 score:143
  // iter:2275 score:145
  // iter:2300 score:166
  // iter:2325 score:119
  // iter:2350 score:252
  // iter:2375 score:85
  // iter:2400 score:128
  // iter:2425 score:151
  // iter:2450 score:160
  // iter:2475 score:119
//dnn_sgd target 5
  // Number of Weights:326
  // iter:0 score:7535
  // iter:25 score:516
  // iter:50 score:376
  // iter:75 score:350
  // iter:100 score:295
  // iter:125 score:301
  // iter:150 score:218
  // iter:175 score:255
  // iter:200 score:196
  // iter:225 score:249
  // iter:250 score:195
  // iter:275 score:164
  // iter:300 score:176
  // iter:325 score:209
  // iter:350 score:225
  // iter:375 score:157
  // iter:400 score:183
  // iter:425 score:188
  // iter:450 score:182
  // iter:475 score:244
  // iter:500 score:81
  // iter:525 score:84
  // iter:550 score:122
  // iter:575 score:121
  // iter:600 score:150
  // iter:625 score:162
  // iter:650 score:209
  // iter:675 score:244
  // iter:700 score:135
  // iter:725 score:93
  // iter:750 score:117
  // iter:775 score:78
  // iter:800 score:114
  // iter:825 score:112
  // iter:850 score:131
  // iter:875 score:183
  // iter:900 score:145
  // iter:925 score:113
  // iter:950 score:96
  // iter:975 score:87
  // iter:1000 score:88
  // iter:1025 score:112
  // iter:1050 score:104
  // iter:1075 score:110
  // iter:1100 score:92
  // iter:1125 score:131
  // iter:1150 score:97
  // iter:1175 score:176
  // iter:1200 score:110
  // iter:1225 score:93
  // iter:1250 score:114
  // iter:1275 score:73
  // iter:1300 score:90
  // iter:1325 score:117
  // iter:1350 score:97
  // iter:1375 score:163
  // iter:1400 score:166
  // iter:1425 score:111
  // iter:1450 score:102
  // iter:1475 score:117
  // iter:1500 score:88
  // iter:1525 score:78
  // iter:1550 score:119
  // iter:1575 score:92
  // iter:1600 score:115
  // iter:1625 score:169
  // iter:1650 score:71
  // iter:1675 score:77
  // iter:1700 score:92
  // iter:1725 score:100
  // iter:1750 score:117
  // iter:1775 score:81
  // iter:1800 score:97
  // iter:1825 score:93
  // iter:1850 score:79
  // iter:1875 score:84
  // iter:1900 score:82
  // iter:1925 score:86
  // iter:1950 score:87
  // iter:1975 score:119
  // iter:2000 score:106
  // iter:2025 score:124
  // iter:2050 score:90
  // iter:2075 score:111
  // iter:2100 score:132
  // iter:2125 score:78
  // iter:2150 score:68
  // iter:2175 score:98
  // iter:2200 score:102
  // iter:2225 score:86
  // iter:2250 score:91
  // iter:2275 score:84
  // iter:2300 score:95
  // iter:2325 score:78
  // iter:2350 score:105
  // iter:2375 score:68
  // iter:2400 score:152
  // iter:2425 score:82
  // iter:2450 score:83
  // iter:2475 score:103


//dnn_cmaes target 1
  // iter:0 score:7191
  // iter:100 score:2809
  // iter:200 score:2809
  // iter:300 score:3091
  // iter:400 score:3515
  // iter:500 score:2859
  // iter:600 score:2629
  // iter:700 score:1527
  // iter:800 score:1445
  // iter:900 score:1498
  // iter:1000 score:2193
  // iter:1100 score:2199
  // iter:1200 score:1046
  // iter:1300 score:1008
  // iter:1400 score:1141
  // iter:1500 score:1034
  // iter:1600 score:1113
  // iter:1700 score:908
  // iter:1800 score:1668
  // iter:1900 score:1101
  // iter:2000 score:870
  // iter:2100 score:652
  // iter:2200 score:1490
  // iter:2300 score:1479
  // iter:2400 score:725
  // iter:2500 score:797
  // iter:2600 score:744
  // iter:2700 score:556
  // iter:2800 score:849
  // iter:2900 score:1052
  // iter:3000 score:2033
  // iter:3100 score:889
  // iter:3200 score:949
  // iter:3300 score:1250
  // iter:3400 score:649
  // iter:3500 score:862
  // iter:3600 score:1601
  // iter:3700 score:503
  // iter:3800 score:453
  // iter:3900 score:456
  // iter:4000 score:555
  // iter:4100 score:915
  // iter:4200 score:616
  // iter:4300 score:561
  // iter:4400 score:1006
  // iter:4500 score:480
  // iter:4600 score:541
  // iter:4700 score:442
  // iter:4800 score:433
  // iter:4900 score:535
  // iter:5000 score:1155
  // iter:5100 score:445
  // iter:5200 score:505
  // iter:5300 score:523
  // iter:5400 score:560
  // iter:5500 score:489
  // iter:5600 score:569
  // iter:5700 score:425
  // iter:5800 score:958
  // iter:5900 score:543
  // iter:6000 score:422
  // iter:6100 score:464
  // iter:6200 score:551
  // iter:6300 score:628
  // iter:6400 score:543
  // iter:6500 score:585
  // iter:6600 score:436
  // iter:6700 score:382
  // iter:6800 score:385
  // iter:6900 score:353
  // iter:7000 score:512
  // iter:7100 score:412
  // iter:7200 score:519
  // iter:7300 score:268
  // iter:7400 score:2101
  // iter:7500 score:426
  // iter:7600 score:361
  // iter:7700 score:301
  // iter:7800 score:414
  // iter:7900 score:335
  // iter:8000 score:507
  // iter:8100 score:317
  // iter:8200 score:387
  // iter:8300 score:325
  // iter:8400 score:245
  // iter:8500 score:394
  // iter:8600 score:408
  // iter:8700 score:285
  // iter:8800 score:246
  // iter:8900 score:769
  // iter:9000 score:365
  // iter:9100 score:281
  // iter:9200 score:254
  // iter:9300 score:325
  // iter:9400 score:388
  // iter:9500 score:380
  // iter:9600 score:219
  // iter:9700 score:291
  // iter:9800 score:273
  // iter:9900 score:333

//dnn_cmaes target 2
  // iter:0 score:7667
  // iter:100 score:2333
  // iter:200 score:3545
  // iter:300 score:2288
  // iter:400 score:2677
  // iter:500 score:2320
  // iter:600 score:2012
  // iter:700 score:2441
  // iter:800 score:1645
  // iter:900 score:2071
  // iter:1000 score:2333
  // iter:1100 score:3033
  // iter:1200 score:1211
  // iter:1300 score:1150
  // iter:1400 score:1299
  // iter:1500 score:618
  // iter:1600 score:7659
  // iter:1700 score:1016
  // iter:1800 score:1656
  // iter:1900 score:1206
  // iter:2000 score:1612
  // iter:2100 score:1639
  // iter:2200 score:1738
  // iter:2300 score:957
  // iter:2400 score:2271
  // iter:2500 score:845
  // iter:2600 score:1154
  // iter:2700 score:2330
  // iter:2800 score:1548
  // iter:2900 score:842
  // iter:3000 score:1151
  // iter:3100 score:743
  // iter:3200 score:2299
  // iter:3300 score:567
  // iter:3400 score:708
  // iter:3500 score:540
  // iter:3600 score:1110
  // iter:3700 score:850
  // iter:3800 score:488
  // iter:3900 score:625
  // iter:4000 score:644
  // iter:4100 score:923
  // iter:4200 score:1773
  // iter:4300 score:389
  // iter:4400 score:1115
  // iter:4500 score:564
  // iter:4600 score:487
  // iter:4700 score:748
  // iter:4800 score:755
  // iter:4900 score:817
  // iter:5000 score:718
  // iter:5100 score:602
  // iter:5200 score:590
  // iter:5300 score:571
  // iter:5400 score:863
  // iter:5500 score:580
  // iter:5600 score:410
  // iter:5700 score:420
  // iter:5800 score:1180
  // iter:5900 score:683
  // iter:6000 score:405
  // iter:6100 score:294
  // iter:6200 score:594
  // iter:6300 score:265
  // iter:6400 score:473
  // iter:6500 score:561
  // iter:6600 score:321
  // iter:6700 score:506
  // iter:6800 score:382
  // iter:6900 score:253
  // iter:7000 score:213
  // iter:7100 score:571
  // iter:7200 score:447
  // iter:7300 score:403
  // iter:7400 score:828
  // iter:7500 score:183
  // iter:7600 score:292
  // iter:7700 score:385
  // iter:7800 score:335
  // iter:7900 score:863
  // iter:8000 score:7475
  // iter:8100 score:358
  // iter:8200 score:339
  // iter:8300 score:296
  // iter:8400 score:193
  // iter:8500 score:223
  // iter:8600 score:150
  // iter:8700 score:231
  // iter:8800 score:347
  // iter:8900 score:238
  // iter:9000 score:564
  // iter:9100 score:1489
  // iter:9200 score:583
  // iter:9300 score:287
  // iter:9400 score:144
  // iter:9500 score:214
  // iter:9600 score:844
  // iter:9700 score:244
  // iter:9800 score:205
  // iter:9900 score:293

//dnn_cmaes target 3
  //   iter:0 score:4978
  // iter:100 score:4505
  // iter:200 score:3940
  // iter:300 score:4155
  // iter:400 score:3182
  // iter:500 score:4082
  // iter:600 score:3220
  // iter:700 score:3862
  // iter:800 score:2531
  // iter:900 score:2587
  // iter:1000 score:2382
  // iter:1100 score:2284
  // iter:1200 score:3236
  // iter:1300 score:2925
  // iter:1400 score:3395
  // iter:1500 score:2708
  // iter:1600 score:2866
  // iter:1700 score:2339
  // iter:1800 score:3431
  // iter:1900 score:2518
  // iter:2000 score:2686
  // iter:2100 score:2005
  // iter:2200 score:2502
  // iter:2300 score:2039
  // iter:2400 score:2573
  // iter:2500 score:2158
  // iter:2600 score:2220
  // iter:2700 score:3201
  // iter:2800 score:2396
  // iter:2900 score:2666
  // iter:3000 score:2115
  // iter:3100 score:2310
  // iter:3200 score:2642
  // iter:3300 score:2457
  // iter:3400 score:2469
  // iter:3500 score:2318
  // iter:3600 score:2487
  // iter:3700 score:2140
  // iter:3800 score:2007
  // iter:3900 score:2665
  // iter:4000 score:2054
  // iter:4100 score:1915
  // iter:4200 score:2116
  // iter:4300 score:2198
  // iter:4400 score:2009
  // iter:4500 score:2228
  // iter:4600 score:2975
  // iter:4700 score:2802
  // iter:4800 score:1998
  // iter:4900 score:2767
  // iter:5000 score:1869
  // iter:5100 score:2063
  // iter:5200 score:1799
  // iter:5300 score:2035
  // iter:5400 score:1827
  // iter:5500 score:1649
  // iter:5600 score:1709
  // iter:5700 score:2662
  // iter:5800 score:1669
  // iter:5900 score:1738
  // iter:6000 score:1524
  // iter:6100 score:1636
  // iter:6200 score:1774
  // iter:6300 score:1601
  // iter:6400 score:1594
  // iter:6500 score:1496
  // iter:6600 score:1514
  // iter:6700 score:1195
  // iter:6800 score:1391
  // iter:6900 score:1420
  // iter:7000 score:1356
  // iter:7100 score:1385
  // iter:7200 score:1117
  // iter:7300 score:1182
  // iter:7400 score:1266
  // iter:7500 score:1985
  // iter:7600 score:1427
  // iter:7700 score:1465
  // iter:7800 score:1162
  // iter:7900 score:1212
  // iter:8000 score:1171
  // iter:8100 score:1269
  // iter:8200 score:1363
  // iter:8300 score:1244
  // iter:8400 score:1288
  // iter:8500 score:1084
  // iter:8600 score:1005
  // iter:8700 score:1335
  // iter:8800 score:1179
  // iter:8900 score:967
  // iter:9000 score:1049
  // iter:9100 score:1043
  // iter:9200 score:1015
  // iter:9300 score:927
  // iter:9400 score:1060
  // iter:9500 score:973
  // iter:9600 score:953
  // iter:9700 score:915
  // iter:9800 score:1064
  // iter:9900 score:1032

//dnn_cmaes target 4
  // iter:0 score:3654
  // iter:100 score:3662
  // iter:200 score:3761
  // iter:300 score:2381
  // iter:400 score:3166
  // iter:500 score:2930
  // iter:600 score:3159
  // iter:700 score:3242
  // iter:800 score:1955
  // iter:900 score:2193
  // iter:1000 score:2459
  // iter:1100 score:1792
  // iter:1200 score:1677
  // iter:1300 score:2123
  // iter:1400 score:1566
  // iter:1500 score:2084
  // iter:1600 score:1739
  // iter:1700 score:1559
  // iter:1800 score:2688
  // iter:1900 score:3921
  // iter:2000 score:1308
  // iter:2100 score:2527
  // iter:2200 score:1826
  // iter:2300 score:1617
  // iter:2400 score:1653
  // iter:2500 score:2763
  // iter:2600 score:1414
  // iter:2700 score:1603
  // iter:2800 score:1159
  // iter:2900 score:1680
  // iter:3000 score:1256
  // iter:3100 score:1563
  // iter:3200 score:1615
  // iter:3300 score:1537
  // iter:3400 score:2101
  // iter:3500 score:1629
  // iter:3600 score:1231
  // iter:3700 score:1288
  // iter:3800 score:2119
  // iter:3900 score:794
  // iter:4000 score:888
  // iter:4100 score:874
  // iter:4200 score:1019
  // iter:4300 score:1890
  // iter:4400 score:1564
  // iter:4500 score:1140
  // iter:4600 score:1536
  // iter:4700 score:1118
  // iter:4800 score:1006
  // iter:4900 score:1492
  // iter:5000 score:1186
  // iter:5100 score:980
  // iter:5200 score:1271
  // iter:5300 score:918
  // iter:5400 score:769
  // iter:5500 score:765
  // iter:5600 score:886
  // iter:5700 score:952
  // iter:5800 score:1087
  // iter:5900 score:1911
  // iter:6000 score:954
  // iter:6100 score:2255
  // iter:6200 score:903
  // iter:6300 score:674
  // iter:6400 score:1305
  // iter:6500 score:755
  // iter:6600 score:696
  // iter:6700 score:1306
  // iter:6800 score:636
  // iter:6900 score:660
  // iter:7000 score:644
  // iter:7100 score:786
  // iter:7200 score:675
  // iter:7300 score:708
  // iter:7400 score:689
  // iter:7500 score:1006
  // iter:7600 score:563
  // iter:7700 score:1100
  // iter:7800 score:802
  // iter:7900 score:753
  // iter:8000 score:2403
  // iter:8100 score:1479
  // iter:8200 score:683
  // iter:8300 score:736
  // iter:8400 score:919
  // iter:8500 score:1132
  // iter:8600 score:1125
  // iter:8700 score:763
  // iter:8800 score:679
  // iter:8900 score:2588
  // iter:9000 score:637
  // iter:9100 score:547
  // iter:9200 score:660
  // iter:9300 score:598
  // iter:9400 score:499
  // iter:9500 score:648
  // iter:9600 score:805
  // iter:9700 score:589
  // iter:9800 score:577
  // iter:9900 score:426


//dnn_cmaes target 5
  //   iter:0 score:2496
  // iter:100 score:7168
  // iter:200 score:3541
  // iter:300 score:2496
  // iter:400 score:2496
  // iter:500 score:6930
  // iter:600 score:2602
  // iter:700 score:2182
  // iter:800 score:2555
  // iter:900 score:2200
  // iter:1000 score:3032
  // iter:1100 score:2509
  // iter:1200 score:1907
  // iter:1300 score:1459
  // iter:1400 score:2356
  // iter:1500 score:1897
  // iter:1600 score:1724
  // iter:1700 score:1491
  // iter:1800 score:1731
  // iter:1900 score:997
  // iter:2000 score:1302
  // iter:2100 score:1409
  // iter:2200 score:1380
  // iter:2300 score:1426
  // iter:2400 score:1360
  // iter:2500 score:978
  // iter:2600 score:1006
  // iter:2700 score:1147
  // iter:2800 score:1110
  // iter:2900 score:1251
  // iter:3000 score:1530
  // iter:3100 score:1143
  // iter:3200 score:1082
  // iter:3300 score:1072
  // iter:3400 score:804
  // iter:3500 score:877
  // iter:3600 score:768
  // iter:3700 score:1062
  // iter:3800 score:771
  // iter:3900 score:945
  // iter:4000 score:1151
  // iter:4100 score:957
  // iter:4200 score:1349
  // iter:4300 score:805
  // iter:4400 score:965
  // iter:4500 score:898
  // iter:4600 score:817
  // iter:4700 score:1461
  // iter:4800 score:910
  // iter:4900 score:1547
  // iter:5000 score:7044
  // iter:5100 score:850
  // iter:5200 score:1996
  // iter:5300 score:910
  // iter:5400 score:1347
  // iter:5500 score:803
  // iter:5600 score:854
  // iter:5700 score:725
  // iter:5800 score:803
  // iter:5900 score:805
  // iter:6000 score:744
  // iter:6100 score:922
  // iter:6200 score:879
  // iter:6300 score:701
  // iter:6400 score:760
  // iter:6500 score:1082
  // iter:6600 score:1482
  // iter:6700 score:821
  // iter:6800 score:667
  // iter:6900 score:843
  // iter:7000 score:893
  // iter:7100 score:1179
  // iter:7200 score:852
  // iter:7300 score:728
  // iter:7400 score:777
  // iter:7500 score:638
  // iter:7600 score:716
  // iter:7700 score:1334
  // iter:7800 score:669
  // iter:7900 score:633
  // iter:8000 score:673
  // iter:8100 score:545
  // iter:8200 score:589
  // iter:8300 score:602
  // iter:8400 score:644
  // iter:8500 score:787
  // iter:8600 score:580
  // iter:8700 score:751
  // iter:8800 score:678
  // iter:8900 score:551
  // iter:9000 score:608
  // iter:9100 score:699
  // iter:9200 score:5326
  // iter:9300 score:745
  // iter:9400 score:682
  // iter:9500 score:576
  // iter:9600 score:975
  // iter:9700 score:1511
  // iter:9800 score:662
  // iter:9900 score:683


