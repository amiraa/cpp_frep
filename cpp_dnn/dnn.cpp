#include <Eigen/Core>
#include <armadillo>
#include <iostream>
#include <vector>

namespace cpp_dnn {
    using scalar=double;
    using MatrixXI = arma::Mat<long long unsigned int>;
    using VectorXf = Eigen::Matrix<scalar, Eigen::Dynamic, 1>;
    using MatrixX = arma::Mat<scalar>;

    //..................................................................................................

    //rename to layer??
    class Node { 
        public:
            int _m;
            int _n;
            MatrixX _A;
            bool linear=false;
            MatrixX _W0;//(n x 1)
            MatrixX _W;//(m x n)
            std::string _name="Node";

            Node():_m(0),_n(0){};

            virtual MatrixX forward(const MatrixX& Z){
                return Z;
            }

            virtual MatrixX backward(const MatrixX dLdA){ 
                return dLdA;
            }

            virtual unsigned getWeightsNum(){
                return this->_W.size()+this->_W0.size();
            }
            virtual int getM(){
                return _m;
            }
            virtual int getN(){
                return _n;
            }
            virtual void setW(const MatrixX& W){
                _W=MatrixX(W);
            }
            virtual void setW0(const MatrixX& W0){
                _W0=MatrixX(W0);
            }
            
            virtual bool isLinear(){
                return this->linear;
            }
            virtual void displayWeights(){
                std::cout<<_W0 <<'\n';
                std::cout<<_W <<'\n';
            }

        

            virtual void sgd_step(const scalar lrate){
            }


            MatrixXI predict (const MatrixX& Ypred) {
                return arma::index_max(Ypred,0) ;
            }

            virtual ~Node() {
                // std::cout<<"deleted_"<<name<<" \n";
            }
        // private:
    };
    //..................................................................................................
    class Linear: public Node {
        public:
            int _m;
            int _n;
            MatrixX _W0;//(n x 1)
            MatrixX _W;//(m x n)
            std::string _name="Linear";
            MatrixX _A;
            MatrixX _dLdW;
            MatrixX _dLdW0;
            bool linear=true;

            Linear():_m(0),_n(0){};
            Linear(int m,int n) {
                _m=m;
                _n=n;
                //  (in size, out size)
                _W0.zeros(_n, 1);
                
                // _W<< 1.24737338 << 0.28295388 << 0.69207227<<arma::endr
                //   << 1.58455078 << 1.32056292 <<-0.69103982<<arma::endr;
                _W.randn(m,n);//change with tf.randomNormal(shape1,0,1.0 * m ** (-.5),'float32',0);
                // _name="Linear("+m+","+n+")";

                
            };

            MatrixX forward(const MatrixX& A){
                _A=MatrixX(A); // (m x b)
                // std::cout << arma::size(_W.t()) << '\n';
                // std::cout << arma::size(_A) << '\n';
                // std::cout << _W.size()<< '\n';
                MatrixX temp=_W.t()*_A;
                return temp.each_col()+_W0 ; // (n x b)
               
            }


            MatrixX backward(const MatrixX dLdZ){ // dLdZ is (n x b)
                _dLdW=_A*dLdZ.t();
                _dLdW0= arma::resize(arma::sum(dLdZ,1),arma::size(_W0));
                return _W *dLdZ;
            }

            unsigned getWeightsNum(){
                return this->_W.size()+this->_W0.size();
            }
            int getM(){
                return _m;
            }
            int getN(){
                return _n;
            }
            void setW(const MatrixX& W){
                _W=MatrixX(W);
            }
            void setW0(const MatrixX& W0){
                _W0=MatrixX(W0);
            }

            bool isLinear(){
                return this->linear;
            }

            void sgd_step(const scalar lrate){
                _W= _W-(_dLdW* lrate);
                _W0= _W0-(_dLdW0* lrate);
            }

            void displayWeights(){
                std::cout<<_W0 <<'\n';
                std::cout<<_W <<'\n';
            }
        // private:
    };

    //..................................................................................................

    class Tanh: public Node {
        public:
            MatrixX _A;
            std::string _name="Tanh";

            Tanh() { };

            MatrixX forward(const MatrixX& Z){
                _A=arma::tanh(Z); 
                return _A;
            }

            MatrixX backward(const MatrixX dLdA){ 
                return dLdA%(1.-_A%_A);
                // (1. + _A) % (1. - _A) * 0.5;
            }
            unsigned getWeightsNum(){
                return this->_W.size()+this->_W0.size();
            }

            void sgd_step(const scalar lrate){
            }
        // private:
    };

    class ReLU: public Node {
        public:
            MatrixX _A;
            std::string _name="Relu";

            ReLU() { };

            MatrixX forward(const MatrixX& Z){
                _A=MatrixX(Z); 
                for (unsigned j=0; j<Z.n_cols; j++)
                    for (unsigned i=0; i<Z.n_rows; i++)
                        _A(i, j) = Z(i, j) > 0.0 ? Z(i, j) : 0.0;
                return _A;
            }

            MatrixX backward(const MatrixX dLdA){ 

                MatrixX temp=MatrixX(dLdA); 
                for (unsigned j=0; j<temp.n_cols; j++)
                    for (unsigned i=0; i<temp.n_rows; i++)
                        temp(i, j) = _A(i, j) <= 0. ? 0.0 : dLdA(i, j);
                return temp;
               
            }
            unsigned getWeightsNum(){
                return this->_W.size()+this->_W0.size();
            }

            void sgd_step(const scalar lrate){
            }
        // private:
    };

    class Sigmoid: public Node {
        public:
            MatrixX _A;
            std::string _name="Sigmoid";

            Sigmoid() { };

            MatrixX forward(const MatrixX& Z){
                _A= 1.0 / (1.0 + arma::exp(-Z));
                return _A; 
            }

            MatrixX backward(const MatrixX& dLdA){ 
                return dLdA % (1. - dLdA); //check wrong probably
            }

            void sgd_step(const scalar lrate){
            }
        // private:
    };

    class SoftMax: public Node {
        public:
            MatrixX _A;
            std::string _name="SoftMax";

            SoftMax() { };

            MatrixX forward(const MatrixX& Z){
                MatrixX temp=arma::sum(arma::exp(Z),0);
                MatrixX temp1=arma::exp(Z);
                _A=temp1.each_row()/temp;
                //.each_col()
                // tf.div(tf.exp(Z) , tf.sum(tf.exp(Z),0));
                return _A;
            }

            MatrixX backward(const MatrixX& dLdZ){ 
                return dLdZ;
            }
            MatrixXI predict (const MatrixX& Ypred) {
                return arma::index_max(Ypred,0) ;
            }

            unsigned getWeightsNum(){
                return this->_W.size()+this->_W0.size();
            }
            

            void sgd_step(const scalar lrate){
            }
        // private:
    };

    //..................................................................................................

    class Loss: public Node { 
        public:
            MatrixX _Ypred;
            MatrixX _Y;
            std::string _name;

            Loss() { };

            virtual MatrixX forward(const MatrixX& Ypred,const MatrixX& Y){

                _Ypred =MatrixX( Ypred);
                _Y = MatrixX(Y);
                
                return -arma::sum( (_Y%arma::log(_Ypred)) ,0);//o?
                //tf.sum(tf.mul(this.Y ,tf.log(this.Ypred))).neg();
            }

            virtual MatrixX backward(){ 
                return _Ypred-_Y;
            }

            virtual void sgd_step(const scalar lrate){
            }
            
        // private:
    };

    class NLL: public Loss {
        public:
            MatrixX _Ypred;
            MatrixX _Y;
            std::string _name="NLL";

            NLL() { };

            MatrixX forward(const MatrixX& Ypred,const MatrixX& Y){

                _Ypred =MatrixX( Ypred);
                _Y = MatrixX(Y);
                
                return -arma::sum( (_Y%arma::log(_Ypred)) ,0);//o?
                //tf.sum(tf.mul(this.Y ,tf.log(this.Ypred))).neg();
            }

            MatrixX backward(){ 
                return _Ypred-_Y;
            }
            // void sgd_step(const scalar lrate){
            // }
            
        // private:
    };

    class Sequential {
        public:
            std::vector<Node*> _modules;
            Loss* _loss;
            scalar _totalLoss=0;
            // std::string _name;

            Sequential(std::vector<Node*> modules,Loss* loss) { 
                _modules=modules;
                _loss=loss;

            };

            MatrixX forward(const MatrixX& Xt){
                MatrixX res=MatrixX(Xt);
                for(unsigned i=0;i< _modules.size();i++ ){
                    res = _modules[i]->forward(res);
                }
                return res;
            }

            MatrixX backward(const MatrixX& delta){ 
                MatrixX res=MatrixX(delta);
                for(int i=_modules.size()-1;i>=0;i=i-1 ){
                    res = _modules[i]->backward(res);
                }
            }

            void sgd_step(const scalar lrate){
                for(unsigned i=0;i< _modules.size();i++ ){
                    _modules[i]->sgd_step(lrate);
                }
                
            }

            MatrixXI step (const MatrixX& Xt,const MatrixX&Yt) { //add argument for weights
                MatrixX Ypred = this->forward(Xt);
                // std::cout << Ypred<< '\n';
                // std::cout <<"Loss:"<<_loss->forward(Ypred, Yt)<< '\n';
                // _totalLoss= arma::accu(_loss->forward(Ypred, Yt));
                MatrixXI pred=this->predict ( Ypred);
                MatrixXI truth=this->predict ( Yt);
                _totalLoss=arma::accu(arma::abs(pred+(-truth)));
                return pred;
            };

            scalar sumLoss(){
                std::cout<<"Loss:"<<_totalLoss <<'\n';
                return _totalLoss;

            }

            MatrixXI predict (const MatrixX& Ypred) {
                return arma::index_max(Ypred,0) ;
            };

            unsigned getWeightsNum(){
                MatrixX _W0;//(n x 1)
                MatrixX _W;//(m x n)
                unsigned res=0;
                for(unsigned i=0;i< _modules.size();i++ ){

                    if(_modules[i]->isLinear()){
                        res += _modules[i]->getWeightsNum();
                    }
                    
                }
                return res;

            }

            std::vector<scalar> slice(std::vector<scalar> const &v, int const m, int const n){
                auto first = v.cbegin() + m;
                auto last = v.cbegin()+ m + n ;

                std::vector<scalar> vec(first, last);
                return vec;
            }

            MatrixX vecToMatrix( std::vector<scalar> const &v,int const m, int const n){
                MatrixX mat;
                mat.zeros(m, n);
                unsigned ii=0;
                for(unsigned i=0;i<m;i++){
                    for(unsigned j=0;j<n;j++){
                        mat(i,j)=v[ii];
                        ii++;
                    }
                }
                return mat;

            }
            
            void displayVector(std::vector<scalar> const &v){

                for(unsigned i=0;i<v.size();i++){
                    std::cout<<v[i] <<" ";
                }
                std::cout<<'\n';
            }

            void setWeights(std::vector<scalar> weightsVector){
                int count=0;
                for(unsigned i=0;i< _modules.size();i++ ){

                    if(_modules[i]->isLinear()){
                        int m=_modules[i]->getM();
                        int n=_modules[i]->getN();
                        int size=m*n;
                        
                        //set W0
                        // displayVector(slice(weightsVector,  count, n));
                        // std::cout<<vecToMatrix( slice(weightsVector,  count, n),n ,1 ) <<'\n';
                        MatrixX newW0= vecToMatrix( slice(weightsVector,  count, n),n ,1 );
                        _modules[i]->setW0(newW0);
                        count += n;

                        //set W
                        // displayVector(slice(weightsVector,  count, size));
                        // std::cout<<vecToMatrix( slice(weightsVector,  count, size),m ,n ) <<'\n';
                        MatrixX newW= vecToMatrix( slice(weightsVector,  count, size),m ,n );
                        _modules[i]->setW(newW);
                        count += (m*n);
                    } 
                }
                // std::cout<<count <<'\n';
            }
            
            
            
            // void sgd(){
            void sgd(const MatrixX& X,const MatrixX& Y, const int iters,const scalar lrate){
                // unsigned D= X.n_cols;
                // unsigned N= X.n_rows;

                unsigned D= X.n_rows;
                unsigned N= X.n_cols;
                unsigned O= Y.n_rows;
                // var D=X.shape[0];
                // var N=X.shape[1];
                int sum_loss=0;
                // var sum_loss=tf.tensor([0]);
                
                for(int i=0;i<iters;i++){
                    unsigned j=(unsigned)(rand() % N);
                    MatrixX Xt= X.submat( 0, j, D-1, j );
                    MatrixX Yt= Y.submat( 0, j, O-1, j );
                    auto Ypred = this->forward(Xt);
                    sum_loss+= arma::accu( _loss->forward(Ypred, Yt) );

                    auto err = _loss->backward();

                    this->backward(err);

                    this->sgd_step(lrate); 
                    
                }
                // std::cout<<"Loss: "<< sum_loss<<std::endl;
                
           
            }
            
        // private:
    };

    //..................................................................................................




}
