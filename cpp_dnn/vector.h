#ifndef CPP_DNN_VECTOR_H
#define CPP_DNN_VECTOR_H

#include <Eigen/Core>

namespace cpp_dnn {

//--------------------------------------------------------------------------------------------------
template <typename T>
using Vector2 = Eigen::Matrix<T, 2, 1>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using Vector3 = Eigen::Matrix<T, 3, 1>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using VectorX = Eigen::Matrix<T, Eigen::Dynamic, 1>;

}

#endif
