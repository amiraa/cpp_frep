#include "frep_tree.cpp"
#include "nelder_mead.h"
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
// #include <nlohmann/json.hpp>



using namespace cpp_frep;
// using json = nlohmann::json;

using scalar=float;
const float  PI_F=3.14159265358979f;


//--------------------------------------------------------------------------------------------------
std::vector<std::string> insert_newlines(const std::string &in, const size_t every_n){
    std::vector<std::string> outStrings;
    int count=0;
    outStrings.push_back("");
    for(std::string::size_type i = 0; i < in.size(); i++) {
        if (!(i % every_n) && i) {
            // out.push_back('\n');
            outStrings.push_back("");
            count++;
        }
        outStrings[count].push_back(in[i]);
    }
    return outStrings;
}

//for frep save and visualize no text
cv::Mat drawFrep(Frep* frep,scalar minX,scalar maxX,scalar minY,scalar maxY,scalar dx,scalar dy,std::string name,bool preview){

    int height=(int)((maxX-minX)/dx);
    int width=(int)((maxY-minY)/dy);
    const int size=height*width*4;
    


    int stride=0;
    unsigned char pix[size];


    for(scalar i=minX;i<maxX-dx;i+=dx){
        for(scalar j=minY;j<maxY-dy;j+=dy){
            frep->setXY(i,j);
            scalar val=frep->eval();
            if(val>0.0f){
                // std::cout << "+" ;
                pix[stride+0]=0;
                pix[stride+1]=0;
                pix[stride+2]=0;
                pix[stride+3]=0;
                
            }else{
                // std::cout << "o" ;
                pix[stride+0]=255;
                pix[stride+1]=255;
                pix[stride+2]=255;
                pix[stride+3]=255;
            }
            stride+=4;
        }
        // std::cout  << '\n';
    }
    // std::cout  << '\n';
    
    // float scale=0.3;
    // int y=20;

    cv::Mat image = cv::Mat(width, height, CV_8UC4, pix);
    // cv::putText(image, 
    //         frep->getPrintedName(),
    //         cv::Point(10,y), // Coordinates
    //         cv::FONT_HERSHEY_SIMPLEX, // Font
    //         scale, // Scale. 2.0 = 2x bigger
    //         cv::Scalar(0,0,0), // BGR Color
    //         1); // Line Thickness (Optional)

    std::string fileName="img_dft/"+name+".png";
    cv::imwrite( fileName, image );
    if(preview){
        cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
        cv::imshow("Display Image", image);
        cv::waitKey(0);

    }
    return image;
}


//get random float between 0 and 1
scalar getRandom(){//between 0 and 1
    return ((float) rand() / (RAND_MAX));
}

scalar getNoise(scalar scale){//between 0 and 1
    return ((scalar) rand() / (RAND_MAX))/scale-scale/2.0;
}

//get random genome
Vector getRandomGenome(int n){
    std::vector<float> v;
    for(int i=0;i<n;++i){
        v.push_back(getRandom());
    }
    return Vector(v);
}

Vector getVector (int n,std::vector<scalar> mag,std::vector<scalar> ampl){
    std::vector<float> v;
    for(int i=0;i<n;++i){
        v.push_back(mag[i]);
    }
    for(int i=0;i<n;++i){
        v.push_back(ampl[i]);
    }
    return Vector(v);
}

std::vector<scalar> getRandomMag(int n){
    std::vector<scalar> v;
    v.push_back(0.0f);
    v.push_back(1.0f);
    for(int i=2;i<n;++i){
        v.push_back(getRandom()/10000.0);
    }
    return v;

    // std::vector<scalar> v;
    // v.push_back(0.0f);
    // scalar orig=getRandom()+0.8f;
    // for(int i=1;i<n;++i){
    //     orig=orig*0.8f;
    //     v.push_back(orig);
    // }
    // return v;
}

std::vector<scalar> getRandomAmpl(int n){
    std::vector<scalar> v;
    v.push_back(0.0f);
    for(int i=1;i<n;++i){
        // v.push_back(getRandom()*PI_F);
        v.push_back(0.0f);
    }
    return v;
}

Vector getParameters(std::vector<std::vector<scalar>> genome,bool noise, scalar scale){
    std::vector<scalar> v;
    for (unsigned int i =0; i<genome.size();++i){
        if (noise){
            v.push_back(genome[i][3]+getNoise(scale));
            v.push_back(genome[i][4]+getNoise(scale));   

        }else{
            v.push_back(genome[i][3]);
            v.push_back(genome[i][4]);
        }
    }
    return Vector(v);
}

void MyPolygon( cv::Mat img, int w, int h) {
  int lineType = 8;
  /** Create some points */
  cv::Point rook_points[1][20];
  rook_points[0][0] = cv::Point( w/4.0, 7*w/8.0 );
  rook_points[0][1] = cv::Point( 3*w/4.0, 7*w/8.0 );
  rook_points[0][2] = cv::Point( 3*w/4.0, 13*w/16.0 );
  rook_points[0][3] = cv::Point( 11*w/16.0, 13*w/16.0 );
  rook_points[0][4] = cv::Point( 19*w/32.0, 3*w/8.0 );
  rook_points[0][5] = cv::Point( 3*w/4.0, 3*w/8.0 );
  rook_points[0][6] = cv::Point( 3*w/4.0, w/8.0 );
  rook_points[0][7] = cv::Point( 26*w/40.0, w/8.0 );
  rook_points[0][8] = cv::Point( 26*w/40.0, w/4.0 );
  rook_points[0][9] = cv::Point( 22*w/40.0, w/4.0 );
  rook_points[0][10] = cv::Point( 22*w/40.0, w/8.0 );
  rook_points[0][11] = cv::Point( 18*w/40.0, w/8.0 );
  rook_points[0][12] = cv::Point( 18*w/40.0, w/4.0 );
  rook_points[0][13] = cv::Point( 14*w/40.0, w/4.0 );
  rook_points[0][14] = cv::Point( 14*w/40.0, w/8.0 );
  rook_points[0][15] = cv::Point( w/4.0, w/8.0 );
  rook_points[0][16] = cv::Point( w/4.0, 3*w/8.0 );
  rook_points[0][17] = cv::Point( 13*w/32.0, 3*w/8.0 );
  rook_points[0][18] = cv::Point( 5*w/16.0, 13*w/16.0 );
  rook_points[0][19] = cv::Point( w/4.0, 13*w/16.0) ;

  const cv::Point* ppt[1] = { rook_points[0] };
  int npt[] = { 20 };

  cv::fillPoly( img,
            ppt,
            npt,
            1,
            cv::Scalar( 255, 255, 255 ),
            lineType );
}

cv::Mat dft(int w, int h, const int resolution, std::vector<scalar> mag,std::vector<scalar> ampl ,std::string name,bool save ,bool preview) {

    cv::Mat img = cv::Mat(w, h, CV_8UC4, cv::Scalar(255,255, 255));
    
    int lineType = 8;

    /** Create some points */
    const int numPoints=100;
    // const int resolution=5;
    int offset=resolution/2;
    const scalar scale=0.5f;

    //get i
    std::complex<scalar> j;
    j=-1.0f;
    j=sqrt(j);

    cv::Point rook_points[1][numPoints];

    // std::vector<scalar> mag;
    // std::vector<scalar> ampl;

    std::vector<scalar> coeff;
    std::vector<std::complex<scalar>> C;

    // for(int i=-offset;i<=offset;i++){
    //     C.push_back(polar (mag[i+offset] *scale, ampl[i+offset]-PI_F/2.0f));
    //     cout << " i: " << i << endl;
    //     coeff.push_back((scalar) i);
    // }

     // mag={10.72f,-12.64f,-135.66f,-44.85f,66.75f};
    // ampl={16.52f,20.9f,-45.57f,-23.71f,-53.07f};
    // C.push_back(std::complex<scalar> (mag[0] , ampl[0]));
    // coeff.push_back(0.0f);

    // int count=1;
    // for(int i=1;i<=offset;i++){
    //     // mag.push_back(0.5f*scale);
    //     // ampl.push_back(0.927295f);
    //     C.push_back(std::complex<scalar> (mag[count] , ampl[count]));
    //     // C.push_back(polar (mag[count] *scale, ampl[count]-PI_F/2.0f));
    //     cout << " i: " << i << endl;
    //     coeff.push_back((scalar) i);
    //     count++;

    //     C.push_back(std::complex<scalar> (mag[count] , ampl[count]));
    //     // C.push_back(polar (mag[count] *scale, ampl[count]-PI_F/2.0f));
    //     cout << " i: " << -i << endl;
    //     coeff.push_back((scalar) -i);
    //     count++;
    // }

    // mag ={0.0f,0.5f,    0.25f,0.1f,0.05f};
    // ampl={0.0f,0.0f,PI_F/2.0f,0.0f,0.0f};



    C.push_back(polar (mag[0] , ampl[0]-PI_F/2.0f));
    coeff.push_back(0.0f);
    int count=1;
    for(int i=1;i<=offset;i++){

        C.push_back(polar (mag[count] , ampl[count]-PI_F/2.0f));
        coeff.push_back((scalar) i);
        count++;

        C.push_back(polar (mag[count] , ampl[count]-PI_F/2.0f));
        coeff.push_back((scalar) -i);
        count++;
    }

    for(int i=0;i<numPoints;i++){
        //sum
        
        scalar t=(scalar)i/ ((scalar)numPoints-1.0f) * 2.0f;
        
        // std::complex<scalar> temp=  exp (1.0f * PI_F * j * t);
        std::complex<scalar> temp(0.0f, 0.0f);

        for(int ii=0;ii<resolution;ii++){
            temp += C[ii]* exp (coeff[ii] * PI_F * j * t);
        }

        
        int x= (int) (real(temp)*scale * (scalar) w /2.0)+w/2;
        int y= (int) (imag(temp)*scale * (scalar) h /2.0)+h/2;

        // cout << " t: " << t;
        // cout << " x: " << x; 
        // cout << " y: " << y << endl;

        rook_points[0][i] = cv::Point(x,y );
        // rook_points[0][i] = cv::Point( w/4.0, 7*w/8.0 );

    }


    const cv::Point* ppt[1] = { rook_points[0] };
    int npt[] = { numPoints };

    cv::fillPoly( img,
                ppt,
                npt,
                1,
                cv::Scalar( 0, 0, 0 ),
                lineType );
    
    std::string fileName="img_dft/"+name+".png";
    cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);
    if(save){
        cv::imwrite ( fileName, img );
    }

    if(preview){
        cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
        cv::imshow("Display Image", img);
        cv::waitKey(0);

    }

    return img;
}

scalar compareToTarget(int width,int height,const int resolution,Vector v, cv::Mat target ,std::string name,bool save,bool preview ){

    std::vector<scalar> mag;
    for(int i=0;i<resolution;++i){
        mag.push_back(v[i]);
    }
    // std::vector<scalar> ampl=getRandomAmpl(resolution);
    std::vector<scalar> ampl;
    for(int i=resolution-1;i<resolution*2;++i){
        ampl.push_back(v[i]);
    }
    
    cv::Mat d = dft(width, height, resolution, mag,ampl,name,save,preview);
    // cv::cvtColor(d, d, cv::COLOR_BGR2GRAY);

    cv::Mat diff= d-target;

    scalar error=(scalar) cv::norm(d, target,cv::NORM_HAMMING );
    // scalar error=(scalar) cv::norm(d, target,cv::NORM_L2 );

    // cout<< "nonzero: "<<countNonZero(diff)<<endl;
    // cout<< "size: "<<width*height<<endl;
    // cout<< "norm l2: "<<cv::norm(d, targ,  cv::NORM_L2)<<endl;
    // cout<< "norm l1: "<<cv::norm(d, targ,  cv::NORM_L1)<<endl;
    // cout << "Error: "<<error<<endl;
    // cout<< "norm NORM_HAMMING2: "<<cv::norm(d, targ,cv::NORM_HAMMING2 )<<endl;
    // cout<< "norm NORM_L2SQR: "<<cv::norm(d, targ,cv::NORM_L2SQR )<<endl;
    // cout<< "norm: "<<cv::norm(d, targ)<<endl;
    


    // cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
    // cv::imshow("Display Image", diff);
    // cv::waitKey(0);

    return -error;


}

int main(int const, char const**) {
   


    float precision = 0.001f;

    
    scalar res=0.05f;
    scalar bound =2.5f;


    
    scalar minX=-bound;
    scalar maxX=bound;
    scalar minY=-bound;
    scalar maxY=bound;
    scalar dx=res;
    scalar dy=res;
    int height=(int)((maxX-minX)/dx);
    int width=(int)((maxY-minY)/dy);

    cout<<"width: "<<width<<", height: "<<height <<std::endl;


    const int resolution=19;

    cpp_frep::Circle c(1.5f,0.0f,0.0f);
    // std::vector<unsigned char> targ1 =drawFrep(&c,minX, maxX, minY, maxY, dx, dy,"circle_target",false);
    cv::Mat targ1 =drawFrep(&c,minX, maxX, minY, maxY, dx, dy,"dft_circle",false);

    cpp_frep::Rectangle r(-1.5f,1.5f,-1.0f,1.0f);
    cpp_frep::Rotate rot1 (&r,0.0f,0.0f,90.0f);
    // std::vector<unsigned char> targ2 =drawFrep(&rot1,minX, maxX, minY, maxY, dx, dy,"rectange_target",false);
    cv::Mat targ2 =drawFrep(&rot1,minX, maxX, minY, maxY, dx, dy,"dft_reactangle",false);

    cpp_frep::InvoluteGear gear( 1.0f,  1.0f, 1.1f, 20.0f, 8.0f) ;
    cpp_frep::Rotate rot (&gear,0.0f,0.0f,15.0f);
    cpp_frep::Scale sc1(&rot,0.0f,0.0f,0.5f,0.5f);
    cpp_frep::Add target(&sc1,&c);
    // std::vector<unsigned char> targ3 =drawFrep(&target,minX, maxX, minY, maxY, dx, dy,"gear_target",false);
    cv::Mat targ3 =drawFrep(&target,minX, maxX, minY, maxY, dx, dy,"dft_gear",false);


    cpp_frep::Circle c1(0.9f,-0.5f,0.8f);
    cpp_frep::Circle c2(0.8f,0.2f,-0.2f);
    cpp_frep::Circle c3(0.9f,1.5f,0.0f);
    cpp_frep::Circle c4(0.9f,1.7f,-1.1f);
    // Subtract ss(&rot1,&c1);
    
    // Subtract sss(&ss,&c2);
    cpp_frep::Add aa(&c1,&c3);
    cpp_frep::Add aaa(&aa,&c2);
    cpp_frep::Add a(&aaa,&c4);
    cpp_frep::Morph m (&a,&target,0.6f);
    cpp_frep::Rotate rot2 (&m,0.3f,0.0f,-90.0f);
    // Scale scc (&m,0.0f,0.0f,1.0,1.0);
    // std::vector<unsigned char> targ4 =drawFrep(&rot2,minX, maxX, minY, maxY, dx, dy,"blob_target",false);
    cv::Mat targ4 =drawFrep(&rot2,minX, maxX, minY, maxY, dx, dy,"dft_blob",false);

    cpp_frep::Circle c5(0.5f,0.0f,0.0f);
    cpp_frep::Subtract ss(&c,&c5);
    // std::vector<unsigned char> targ5 =drawFrep(&ss,minX, maxX, minY, maxY, dx, dy,"hole_target",false);
    cv::Mat targ5 =drawFrep(&ss,minX, maxX, minY, maxY, dx, dy,"dft_hole",false);




    // Circle c(1.2f,0.0f,0.0f);
    // InvoluteGear gear( 1.0f,  1.0f, 1.1f, 20.0f, 8.0f) ;
    // Rotate rot (&gear,0.0f,0.0f,15.0f);
    // Scale sc1(&rot,0.0f,0.0f,0.3f,0.3f);
    // Add target(&sc1,&c);
    cv::Mat targ;

    // Circle c(1.0f,0.0f,0.0f);
    // cv::Mat targ =drawFrep(&c,minX, maxX, minY, maxY, dx, dy,"dft_target",false);
    cv::cvtColor(targ3, targ, cv::COLOR_BGR2GRAY);




    ///neldermead

    // scalar precision = 0.001f;
    int dimension = resolution;
    NelderMeadOptimizer o(dimension, precision);

    // request a simplex to start with
    // Vector v(0.5, 0.5);
    Vector v =getVector (resolution,getRandomMag(resolution),getRandomAmpl(resolution));

    // scalar er=compareToTarget( width, height,resolution,v, targ, "dft" ,true);

    Vector v1 =getVector (resolution,getRandomMag(resolution),getRandomAmpl(resolution));
    Vector v2 =getVector (resolution,getRandomMag(resolution),getRandomAmpl(resolution));

    o.insert(v);
    o.insert(v1);
    o.insert(v2);

    int count=0;
    int count1=0;
    int count2=0;
    int maxSteps=10000;
    int maxSteps1=10000;
    int saveEvery=25;
    scalar score=0.0f;

    std::string name1 = "ii";
    while (!o.done(score)&& count1<maxSteps1) {
        if(count>maxSteps){
            count=0;
            count2=0;
            std::cout<<"Counld't find solution, reset with random values!!!!!!!!!!!!!!!!!!!\n";
            o.reset();
            o.insert(v);
            o.insert(getVector (resolution,getRandomMag(resolution),getRandomAmpl(resolution)));
            o.insert(getVector (resolution,getRandomMag(resolution),getRandomAmpl(resolution)));
        }

        std::string name=name1+std::to_string(count);
        if(count2>=saveEvery){
            //save
            score=compareToTarget( width, height,resolution,v, targ ,name,true,false);
            std::cout<<"Step:"<<count<<", total:"<< count1<<", score:"<<-score<<"\n";
            count2=0;

        }else{
            score=compareToTarget( width, height,resolution,v, targ ,name,false,false);
            count2++;

        }
            
        
        v = o.step(v, score);
        count++;
        count1++;

    }
    
    

    std::cout<<"Vector: ";
    for (int i;i<resolution;i++){
        cout<<v[i]<<" ";
    }

    std::cout<<"Final: i"<<count<<"\n";

    score=compareToTarget( width, height,resolution,v, targ ,"final",true,true);
   


    
    //0 add, 1 subtract, 2 intersect, 3 morph, 4 translate, 5 scale, 6 rotate, 7 linearArray, 8 polar array

    return 0;
}


///target1
    // Step:25, total:25, score:44004
    // Step:51, total:51, score:40950
    // Step:77, total:77, score:26444
    // Step:103, total:103, score:23756
    // Step:129, total:129, score:16948
    // Step:155, total:155, score:14380
    // Step:181, total:181, score:13500
    // Step:207, total:207, score:12524
    // Step:233, total:233, score:13532
    // Step:259, total:259, score:12020
    // Step:285, total:285, score:12356
    // Step:311, total:311, score:11908
    // Step:337, total:337, score:11772
    // Step:363, total:363, score:12788
    // Step:389, total:389, score:11564
    // Step:415, total:415, score:12196
    // Step:441, total:441, score:11636
    // Step:467, total:467, score:12268
    // Step:493, total:493, score:11724
    // Step:519, total:519, score:11796
    // Step:545, total:545, score:11932
    // Step:571, total:571, score:12180
    // Step:597, total:597, score:11740
    // Step:623, total:623, score:11812
    // Step:649, total:649, score:12172
    // Step:675, total:675, score:11764
    // Step:701, total:701, score:11972
    // Step:727, total:727, score:11836
    // Step:753, total:753, score:11740
    // Step:779, total:779, score:11716
    // Step:805, total:805, score:11732
    // Step:831, total:831, score:11812
    // Step:857, total:857, score:11788
    // Step:883, total:883, score:11740
    // Step:909, total:909, score:11860
    // Step:935, total:935, score:11908
    // Step:961, total:961, score:11748
    // Step:987, total:987, score:11948
    // Step:1013, total:1013, score:11676
    // Step:1039, total:1039, score:12260
    // Step:1065, total:1065, score:11764
    // Step:1091, total:1091, score:11772
    // Step:1117, total:1117, score:11764
    // Step:1143, total:1143, score:11788
    // Step:1169, total:1169, score:11972
    // Step:1195, total:1195, score:11804
    // Step:1221, total:1221, score:11884
    // Step:1247, total:1247, score:11900
    // Step:1273, total:1273, score:11868
    // Step:1299, total:1299, score:12260
    // Step:1325, total:1325, score:11732
    // Step:1351, total:1351, score:11740
    // Step:1377, total:1377, score:11900
    // Step:1403, total:1403, score:11796
    // Step:1429, total:1429, score:11740
    // Step:1455, total:1455, score:11788
    // Step:1481, total:1481, score:12236

//target 2

    // Step:25, total:25, score:28356
    // Step:51, total:51, score:23300
    // Step:77, total:77, score:24778
    // Step:103, total:103, score:18426
    // Step:129, total:129, score:16386
    // Step:155, total:155, score:14418
    // Step:181, total:181, score:13234
    // Step:207, total:207, score:12794
    // Step:233, total:233, score:12426
    // Step:259, total:259, score:12746
    // Step:285, total:285, score:11562
    // Step:311, total:311, score:11978
    // Step:337, total:337, score:11786
    // Step:363, total:363, score:11282
    // Step:389, total:389, score:12162
    // Step:415, total:415, score:12074
    // Step:441, total:441, score:12218
    // Step:467, total:467, score:11530
    // Step:493, total:493, score:11754
    // Step:519, total:519, score:11994
    // Step:545, total:545, score:11810
    // Step:571, total:571, score:11658
    // Step:597, total:597, score:11218
    // Step:623, total:623, score:11898
    // Step:649, total:649, score:11506
    // Step:675, total:675, score:11442
    // Step:701, total:701, score:11474
    // Step:727, total:727, score:11970
    // Step:753, total:753, score:12042
    // Step:779, total:779, score:11594
    // Step:805, total:805, score:11986
    // Step:831, total:831, score:11450
    // Step:857, total:857, score:11762
    // Step:883, total:883, score:11522
    // Step:909, total:909, score:11442
    // Step:935, total:935, score:11890
    // Step:961, total:961, score:11490
    // Step:987, total:987, score:11986
    // Step:1013, total:1013, score:11706
    // Step:1039, total:1039, score:12186
    // Step:1065, total:1065, score:11610
    // Step:1091, total:1091, score:11866
    // Step:1117, total:1117, score:11626
    // Step:1143, total:1143, score:12106
    // Step:1169, total:1169, score:11386
    // Step:1195, total:1195, score:11450
    // Step:1221, total:1221, score:11586
    // Step:1247, total:1247, score:11418
    // Step:1273, total:1273, score:11450
    // Step:1299, total:1299, score:11610
    // Step:1325, total:1325, score:11898
    // Step:1351, total:1351, score:11658
    // Step:1377, total:1377, score:11658
    // Step:1403, total:1403, score:11746
    // Step:1429, total:1429, score:11818
    // Step:1455, total:1455, score:11730
    // Step:1481, total:1481, score:11442
    // Step:1507, total:1507, score:11786
    // Step:1533, total:1533, score:11442
    // Step:1559, total:1559, score:11874
    // Step:1585, total:1585, score:11714
    // Step:1611, total:1611, score:11626
    // Step:1637, total:1637, score:11458
    // Step:1663, total:1663, score:11178
    // Step:1689, total:1689, score:12146
    // Step:1715, total:1715, score:11634
    // Step:1741, total:1741, score:12178
    // Step:1767, total:1767, score:11850
    // Step:1793, total:1793, score:11698
    // Step:1819, total:1819, score:11634
    // Step:1845, total:1845, score:11570
    // Step:1871, total:1871, score:12026
    // Step:1897, total:1897, score:11610
    // Step:1923, total:1923, score:11490
    // Step:1949, total:1949, score:12122
    // Step:1975, total:1975, score:11914
    // Step:2001, total:2001, score:11522
    // Step:2027, total:2027, score:12314
    // Step:2053, total:2053, score:11594
    // Step:2079, total:2079, score:11866
    // Step:2105, total:2105, score:11538
    // Step:2131, total:2131, score:11362
    // Step:2157, total:2157, score:11514
    // Step:2183, total:2183, score:11650
    // Step:2209, total:2209, score:11514
    // Step:2235, total:2235, score:11554
    // Step:2261, total:2261, score:11474
    // Step:2287, total:2287, score:11954
    // Step:2313, total:2313, score:11594
    // Step:2339, total:2339, score:12098
    // Step:2365, total:2365, score:11930
    // Step:2391, total:2391, score:11746
    // Step:2417, total:2417, score:11226
    // Step:2443, total:2443, score:11506
    // Step:2469, total:2469, score:11714
    // Step:2495, total:2495, score:11786
    // Step:2521, total:2521, score:12338
    // Step:2547, total:2547, score:11618

// target3

// target4


// target5

