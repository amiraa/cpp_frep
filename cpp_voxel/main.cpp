#include "frep_tree.cpp"
#include "nelder_mead.h"
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
// #include <nlohmann/json.hpp>



using namespace cpp_frep;
// using json = nlohmann::json;

using scalar=float;
const float  PI_F=3.14159265358979f;


//--------------------------------------------------------------------------------------------------
std::vector<std::string> insert_newlines(const std::string &in, const size_t every_n){
    std::vector<std::string> outStrings;
    int count=0;
    outStrings.push_back("");
    for(std::string::size_type i = 0; i < in.size(); i++) {
        if (!(i % every_n) && i) {
            // out.push_back('\n');
            outStrings.push_back("");
            count++;
        }
        outStrings[count].push_back(in[i]);
    }
    return outStrings;
}

//for frep save and visualize no text
std::vector<unsigned char> drawFrep(Frep* frep,scalar minX,scalar maxX,scalar minY,scalar maxY,scalar dx,scalar dy,std::string name,bool preview){

    int height=(int)((maxX-minX)/dx);
    int width=(int)((maxY-minY)/dy);
    const int size=height*width*4;
    
    cout<<"width: "<<width<<", height: "<<height <<std::endl;

    int stride=0;
    std::vector<unsigned char> pix1(size,0);
    unsigned char pix[size];


    for(scalar i=minX;i<maxX-dx;i+=dx){
        for(scalar j=minY;j<maxY-dy;j+=dy){
            frep->setXY(i,j);
            scalar val=frep->eval();
            if(val>0.0f){
                // std::cout << "+" ;
                pix[stride+0]=0;
                pix[stride+1]=0;
                pix[stride+2]=0;
                pix[stride+3]=0;
                pix1[stride+0]=0;
                pix1[stride+1]=0;
                pix1[stride+2]=0;
                pix1[stride+3]=0;
                
            }else{
                // std::cout << "o" ;
                pix[stride+0]=255;
                pix[stride+1]=255;
                pix[stride+2]=255;
                pix[stride+3]=255;
                pix1[stride+0]=255;
                pix1[stride+1]=255;
                pix1[stride+2]=255;
                pix1[stride+3]=255;
            }
            stride+=4;
        }
        // std::cout  << '\n';
    }
    // std::cout  << '\n';
    
    // float scale=0.3;
    // int y=20;
    cout<<sizeof(pix)/sizeof(pix[0])<<endl;
    cv::Mat image = cv::Mat(width, height, CV_8UC4, pix);
    // cv::putText(image, 
    //         frep->getPrintedName(),
    //         cv::Point(10,y), // Coordinates
    //         cv::FONT_HERSHEY_SIMPLEX, // Font
    //         scale, // Scale. 2.0 = 2x bigger
    //         cv::Scalar(0,0,0), // BGR Color
    //         1); // Line Thickness (Optional)

    std::string fileName="img_voxel/"+name+".png";
    cv::imwrite( fileName, image );
    if(preview){
        cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
        cv::imshow("Display Image", image);
        cv::waitKey(0);

    }
    return pix1;
    // return image;
}

//get random float between 0 and 1
scalar getRandom(){//between 0 and 1
    return ((float) rand() / (RAND_MAX));
}



void EM(int w,int h, std::vector<unsigned char> img){
    //
    // define variables
    //
    // var img = new Uint8ClampedArray(evt.data.image)
    int size = 250; // sampling population size
    int loop = 2500; // number of EM iterations
    
    // var w = img.width // image width
    // var h = img.height // image height

    // loop=10;

    scalar prob[w*h]; // pixel probabilities

    for (int x = 0; x < w; ++x){
        for (int y = 0; y < h; ++y){
            prob[w*y+x] = 0.5;
        }
    }
        
            
    //
    // start the search
    //
    while (loop > 0){
        // cout<<"loop:"<<loop<<std::endl;
        //
        // expectation-maximization step
        //

        //
        // sample population from probabilities
        //
        // std::vector<unsigned char *> pop ;
        std::vector<std::vector<unsigned char>> pop ;
        for (int n = 0; n < size; ++n) {
            std::vector<unsigned char> pix(4*w*h, 0);
            // unsigned char pix[4*w*h];
            pop.push_back(pix);
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {
                    if (getRandom() > prob[w*y+x]){
                        pop[n][w*y*4+x*4+0] = 255;
                        pop[n][w*y*4+x*4+1] = 255;
                        pop[n][w*y*4+x*4+2] = 255;
                        pop[n][w*y*4+x*4+3] = 255;

                    } else{
                        pop[n][w*y*4+x*4+0] = 0;
                        pop[n][w*y*4+x*4+1] = 0;
                        pop[n][w*y*4+x*4+2] = 0;
                        pop[n][w*y*4+x*4+3] = 0;

                    }
                }
            }
        }

        //
        // evaluate population
        //
        std::vector<int>  d ;
        // int dmin = std::numeric_limits<int>::max();
        // int dmax = -std::numeric_limits<int>::max();
        int dmin = w*h*4*2;
        int dmax = -1;
        int nmin,nmax;
        for (int n = 0; n < size; ++n) {
            d.push_back(0);
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {
                    if ((int)pop[n][w*y*4+x*4+0] != (int)img[w*y*4+x*4+0]){
                        d[n] += 1;
                    }  
                }
            }
            if (d[n] < dmin) {
                dmin = d[n];
                nmin = n;
            }
            if (d[n] > dmax) {
                dmax = d[n];
                nmax = n;
            }
        }

        //
        // update normalization
        //
        scalar norm = 0;
        for (int n = 0; n < size; ++n){
            norm += (scalar) (dmax-d[n]);
        }
        //
        // update probabilities
        //
        scalar p;
        scalar rate = 0.25;
        unsigned char pixx[4*w*h];
        for (int x = 0; x < w; ++x) {
            for (int y = 0; y < h; ++y) {
                p = 0.0;

                pixx[w*y*4+x*4+0] =pop[nmin][w*y*4+x*4+0];
                pixx[w*y*4+x*4+1] =pop[nmin][w*y*4+x*4+1];
                pixx[w*y*4+x*4+2] =pop[nmin][w*y*4+x*4+2];
                pixx[w*y*4+x*4+3] =pop[nmin][w*y*4+x*4+3];

                for (int n = 0; n < size; ++n) {
                    if ((int)pop[n][w*y*4+x*4+0] == 0){
                        p += (scalar)(dmax-d[n])/norm;
                    }
                }
                prob[w*y+x] = (1.0-rate)*prob[w*y+x]+rate*p; // filter with learning rate
            }
        }

        if((loop)%50==0){
            cout<<"itr:"<<loop;
            cout<<", distance:"<<dmin<<std::endl;
            cv::Mat image = cv::Mat(w, h, CV_8UC4, pixx);
            std::string fileName="img_voxel/"+std::to_string(loop)+".png";
            cv::imwrite( fileName, image );

        }
        

        //
        // decrement and return
        //
        loop -= 1;
    }
}

void EM_df(int w,int h, std::vector<unsigned char> img){
    //
    // define variables
    //
    // var img = new Uint8ClampedArray(evt.data.image)
    int size = 250; // sampling population size
    int loop = 3000; // number of EM iterations
    int count=0;    
    int factor=5;
    int w1 = w/factor +1;
    int h1 = h/factor +1;

    cout<<"w1: "<<w1<<", h1: "<<h1 <<std::endl;

    // loop=10;

    scalar prob[w1*h1]; // pixel probabilities

    for (int x = 0; x < w1; ++x){
        for (int y = 0; y < h1; ++y){
            prob[w1*y+x] = 0.5;
        }
    }
        
            
    //
    // start the search
    //
    while (count<=loop ){
        // cout<<"loop:"<<loop<<std::endl;
        //
        // expectation-maximization step
        //

        //
        // sample population from probabilities
        //
        // std::vector<unsigned char *> pop ;
        std::vector<std::vector<unsigned char>> pop ;
        std::vector<std::vector<scalar>> popProb ;
        for (int n = 0; n < size; ++n) {
            std::vector<unsigned char> pix(4*w*h, 0);
            pop.push_back(pix);

            std::vector<scalar> pix1(w1*h1, 0.0);
            popProb.push_back(pix1);

            for (int x = 0; x < w1; ++x) {
                for (int y = 0; y < h1; ++y) {
                    if (getRandom() > prob[w1*y+x]){
                        popProb[n][w1*y+x]= 0.5+(getRandom()/2.0);
                    }
                    else{
                        popProb[n][w1*y+x]= getRandom()/2.0;
                    }
                        
                }
            }
        }

        for (int n = 0; n < size; ++n) {
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    //bilinear interpolation
                    int X=int((float) x/(float)factor);
                    int Y=int((float) y/(float)factor);
                    int x1=X*factor;
                    int x2=x1+factor;
                    int y1=Y*factor;
                    int y2=y1+factor;
                    scalar Q11=popProb[n][w1*(Y)+(X)];
                    scalar Q12=popProb[n][w1*(Y+1)+(X)];
                    scalar Q21=popProb[n][w1*(Y)+(X+1)];
                    scalar Q22=popProb[n][w1*(Y+1)+(X+1)];
                    scalar val=1.0/((x2-x1)*(y2-y1))*( Q11*(x2-x)*(y2-y) + Q21*(x-x1)*(y2-y) + Q12*(x2-x)*(y-y1) + Q22*(x-x1)*(y-y1) );

                    if (val > 0.5){
                        pop[n][w*y*4+x*4+0] = 255;
                        pop[n][w*y*4+x*4+1] = 255;
                        pop[n][w*y*4+x*4+2] = 255;
                        pop[n][w*y*4+x*4+3] = 255;

                    } else{
                        pop[n][w*y*4+x*4+0] = 0;
                        pop[n][w*y*4+x*4+1] = 0;
                        pop[n][w*y*4+x*4+2] = 0;
                        pop[n][w*y*4+x*4+3] = 0;

                    }
                }
            }
        }
        //
        // evaluate population
        //
        std::vector<int>  d ;
        // int dmin = std::numeric_limits<int>::max();
        // int dmax = -std::numeric_limits<int>::max();
        int dmin = w*h*4*2;
        int dmax = -1;
        int nmin,nmax;
        for (int n = 0; n < size; ++n) {
            d.push_back(0);
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {
                    if ((int)pop[n][w*y*4+x*4+0] != (int)img[w*y*4+x*4+0]){
                        d[n] += 1;
                    }  
                }
            }
            if (d[n] < dmin) {
                dmin = d[n];
                nmin = n;
            }
            if (d[n] > dmax) {
                dmax = d[n];
                nmax = n;
            }
        }

        //
        // update normalization
        //
        scalar norm = 0;
        for (int n = 0; n < size; ++n){
            norm += (scalar) (dmax-d[n]);
        }
        //
        // update probabilities
        //
        scalar p;
        scalar rate = 0.25;
        unsigned char pixx[4*w*h];
        for (int x = 0; x < w; ++x) {
            for (int y = 0; y < h; ++y) {

                pixx[w*y*4+x*4+0] =pop[nmin][w*y*4+x*4+0];
                pixx[w*y*4+x*4+1] =pop[nmin][w*y*4+x*4+1];
                pixx[w*y*4+x*4+2] =pop[nmin][w*y*4+x*4+2];
                pixx[w*y*4+x*4+3] =pop[nmin][w*y*4+x*4+3];
            }
        }
        for (int x = 0; x < w1; ++x) {
            for (int y = 0; y < h1; ++y) {
                p = 0.0;

                for (int n = 0; n < size; ++n) {
                    if ( popProb[n][w1*y+x]<0.5){
                        p += (scalar)(dmax-d[n])/norm;
                    }
                }
                prob[w1*y+x] = (1.0-rate)*prob[w1*y+x]+rate*p; // filter with learning rate
            }
        }

        if((count)%50==0){
            cout<<"itr:"<<count;
            cout<<", distance:"<<dmin<<std::endl;
            cv::Mat image = cv::Mat(w, h, CV_8UC4, pixx);
            std::string fileName="img_voxel/"+std::to_string(count)+".png";
            cv::imwrite( fileName, image );

        }
        

        //
        // decrement and return
        //
        count++;
    }
}


int main(int const, char const**) {
   
    scalar res=0.05f;
    scalar bound =2.5f;


    
    scalar minX=-bound;
    scalar maxX=bound;
    scalar minY=-bound;
    scalar maxY=bound;
    scalar dx=res;
    scalar dy=res;
    int height=(int)((maxX-minX)/dx);
    int width=(int)((maxY-minY)/dy);

    cout<<"width: "<<width<<", height: "<<height <<std::endl;

    


    
    
    Circle c(1.5f,0.0f,0.0f);
    std::vector<unsigned char> targ1 =drawFrep(&c,minX, maxX, minY, maxY, dx, dy,"circle_target",false);

    Rectangle r(-1.5f,1.5f,-1.0f,1.0f);
    Rotate rot1 (&r,0.0f,0.0f,90.0f);
    std::vector<unsigned char> targ2 =drawFrep(&rot1,minX, maxX, minY, maxY, dx, dy,"rectange_target",false);

    InvoluteGear gear( 1.0f,  1.0f, 1.1f, 20.0f, 8.0f) ;
    Rotate rot (&gear,0.0f,0.0f,15.0f);
    Scale sc1(&rot,0.0f,0.0f,0.5f,0.5f);
    Add target(&sc1,&c);
    std::vector<unsigned char> targ3 =drawFrep(&target,minX, maxX, minY, maxY, dx, dy,"gear_target",false);



    Circle c1(0.9f,-0.5f,0.8f);
    Circle c2(0.8f,0.2f,-0.2f);
    Circle c3(0.9f,1.5f,0.0f);
    Circle c4(0.9f,1.7f,-1.1f);
    // Subtract ss(&rot1,&c1);
    
    // Subtract sss(&ss,&c2);
    Add aa(&c1,&c3);
    Add aaa(&aa,&c2);
    Add a(&aaa,&c4);
    Morph m (&a,&target,0.6f);
    Rotate rot2 (&m,0.3f,0.0f,-90.0f);
    // Scale scc (&m,0.0f,0.0f,1.0,1.0);
    std::vector<unsigned char> targ4 =drawFrep(&rot2,minX, maxX, minY, maxY, dx, dy,"blob_target",false);

    Circle c5(0.5f,0.0f,0.0f);
    Subtract ss(&c,&c5);
    std::vector<unsigned char> targ5 =drawFrep(&ss,minX, maxX, minY, maxY, dx, dy,"hole_target",false);
    

    EM( width, height,  targ3);


    // EM_df( width, height,  targ5);

   


    
    //0 add, 1 subtract, 2 intersect, 3 morph, 4 translate, 5 scale, 6 rotate, 7 linearArray, 8 polar array

    return 0;
}


//voxel target1
    // itr:2500, distance:4886
    // itr:2450, distance:4637
    // itr:2400, distance:4429
    // itr:2350, distance:4218
    // itr:2300, distance:3968
    // itr:2250, distance:3767
    // itr:2200, distance:3553
    // itr:2150, distance:3386
    // itr:2100, distance:3162
    // itr:2050, distance:2965
    // itr:2000, distance:2804
    // itr:1950, distance:2580
    // itr:1900, distance:2423
    // itr:1850, distance:2246
    // itr:1800, distance:2056
    // itr:1750, distance:1929
    // itr:1700, distance:1765
    // itr:1650, distance:1602
    // itr:1600, distance:1457
    // itr:1550, distance:1340
    // itr:1500, distance:1182
    // itr:1450, distance:1051
    // itr:1400, distance:947
    // itr:1350, distance:847
    // itr:1300, distance:716
    // itr:1250, distance:631
    // itr:1200, distance:518
    // itr:1150, distance:447
    // itr:1100, distance:376
    // itr:1050, distance:292
    // itr:1000, distance:220
    // itr:950, distance:165
    // itr:900, distance:123
    // itr:850, distance:77
    // itr:800, distance:44
    // itr:750, distance:17
    // itr:700, distance:6
    // itr:650, distance:1
    // itr:600, distance:1
    // itr:550, distance:1
    // itr:500, distance:1
    // itr:450, distance:1
    // itr:400, distance:1
    // itr:350, distance:1
    // itr:300, distance:1
    // itr:250, distance:1
    // itr:200, distance:1
    // itr:150, distance:1
    // itr:100, distance:1
    // itr:50, distance:1

//voxel target 2
    // itr:2500, distance:4848
    // itr:2450, distance:4619
    // itr:2400, distance:4413
    // itr:2350, distance:4181
    // itr:2300, distance:3988
    // itr:2250, distance:3774
    // itr:2200, distance:3571
    // itr:2150, distance:3360
    // itr:2100, distance:3155
    // itr:2050, distance:2976
    // itr:2000, distance:2783
    // itr:1950, distance:2550
    // itr:1900, distance:2404
    // itr:1850, distance:2217
    // itr:1800, distance:2101
    // itr:1750, distance:1909
    // itr:1700, distance:1753
    // itr:1650, distance:1609
    // itr:1600, distance:1456
    // itr:1550, distance:1308
    // itr:1500, distance:1191
    // itr:1450, distance:1050
    // itr:1400, distance:941
    // itr:1350, distance:804
    // itr:1300, distance:720
    // itr:1250, distance:622
    // itr:1200, distance:515
    // itr:1150, distance:451
    // itr:1100, distance:372
    // itr:1050, distance:287
    // itr:1000, distance:219
    // itr:950, distance:165
    // itr:900, distance:124
    // itr:850, distance:80
    // itr:800, distance:47
    // itr:750, distance:26
    // itr:700, distance:12
    // itr:650, distance:6
    // itr:600, distance:6
    // itr:550, distance:6
    // itr:500, distance:6
    // itr:450, distance:6
    // itr:400, distance:6
    // itr:350, distance:6
    // itr:300, distance:6
    // itr:250, distance:6
    // itr:200, distance:6
    // itr:150, distance:6
    // itr:100, distance:6
    // itr:50, distance:6

//voxel target 3
    //     itr:2500, distance:4875
    // itr:2450, distance:4634
    // itr:2400, distance:4407
    // itr:2350, distance:4196
    // itr:2300, distance:3976
    // itr:2250, distance:3803
    // itr:2200, distance:3578
    // itr:2150, distance:3374
    // itr:2100, distance:3151
    // itr:2050, distance:2964
    // itr:2000, distance:2762
    // itr:1950, distance:2554
    // itr:1900, distance:2399
    // itr:1850, distance:2205
    // itr:1800, distance:2060
    // itr:1750, distance:1885
    // itr:1700, distance:1746
    // itr:1650, distance:1563
    // itr:1600, distance:1447
    // itr:1550, distance:1288
    // itr:1500, distance:1122
    // itr:1450, distance:1020
    // itr:1400, distance:938
    // itr:1350, distance:813
    // itr:1300, distance:683
    // itr:1250, distance:593
    // itr:1200, distance:514
    // itr:1150, distance:427
    // itr:1100, distance:343
    // itr:1050, distance:272
    // itr:1000, distance:206
    // itr:950, distance:152
    // itr:900, distance:105
    // itr:850, distance:66
    // itr:800, distance:39
    // itr:750, distance:15
    // itr:700, distance:2
    // itr:650, distance:0
    // itr:600, distance:0
    // itr:550, distance:0
    // itr:500, distance:0
    // itr:450, distance:0
    // itr:400, distance:0
    // itr:350, distance:0
    // itr:300, distance:0
    // itr:250, distance:0
    // itr:200, distance:0
    // itr:150, distance:0
    // itr:100, distance:0
    // itr:50, distance:0

//voxel target 4
    // itr:2500, distance:4875
    // itr:2450, distance:4553
    // itr:2400, distance:4382
    // itr:2350, distance:4163
    // itr:2300, distance:3945
    // itr:2250, distance:3755
    // itr:2200, distance:3574
    // itr:2150, distance:3351
    // itr:2100, distance:3144
    // itr:2050, distance:2973
    // itr:2000, distance:2761
    // itr:1950, distance:2591
    // itr:1900, distance:2401
    // itr:1850, distance:2226
    // itr:1800, distance:2085
    // itr:1750, distance:1922
    // itr:1700, distance:1744
    // itr:1650, distance:1597
    // itr:1600, distance:1438
    // itr:1550, distance:1319
    // itr:1500, distance:1149
    // itr:1450, distance:1056
    // itr:1400, distance:928
    // itr:1350, distance:780
    // itr:1300, distance:685
    // itr:1250, distance:612
    // itr:1200, distance:507
    // itr:1150, distance:439
    // itr:1100, distance:359
    // itr:1050, distance:288
    // itr:1000, distance:211
    // itr:950, distance:157
    // itr:900, distance:114
    // itr:850, distance:65
    // itr:800, distance:39
    // itr:750, distance:20
    // itr:700, distance:2
    // itr:650, distance:0
    // itr:600, distance:0
    // itr:550, distance:0
    // itr:500, distance:0
    // itr:450, distance:0
    // itr:400, distance:0
    // itr:350, distance:0
    // itr:300, distance:0
    // itr:250, distance:0
    // itr:200, distance:0
    // itr:150, distance:0
    // itr:100, distance:0
    // itr:50, distance:0

//voxel target 5
    // itr:2500, distance:4886
    // itr:2450, distance:4645
    // itr:2400, distance:4403
    // itr:2350, distance:4206
    // itr:2300, distance:3949
    // itr:2250, distance:3764
    // itr:2200, distance:3511
    // itr:2150, distance:3362
    // itr:2100, distance:3181
    // itr:2050, distance:2931
    // itr:2000, distance:2811
    // itr:1950, distance:2585
    // itr:1900, distance:2431
    // itr:1850, distance:2236
    // itr:1800, distance:2094
    // itr:1750, distance:1925
    // itr:1700, distance:1741
    // itr:1650, distance:1603
    // itr:1600, distance:1447
    // itr:1550, distance:1329
    // itr:1500, distance:1198
    // itr:1450, distance:1066
    // itr:1400, distance:953
    // itr:1350, distance:824
    // itr:1300, distance:718
    // itr:1250, distance:626
    // itr:1200, distance:522
    // itr:1150, distance:457
    // itr:1100, distance:366
    // itr:1050, distance:291
    // itr:1000, distance:218
    // itr:950, distance:170
    // itr:900, distance:120
    // itr:850, distance:77
    // itr:800, distance:43
    // itr:750, distance:22
    // itr:700, distance:8
    // itr:650, distance:3
    // itr:600, distance:2
    // itr:550, distance:2
    // itr:500, distance:2
    // itr:450, distance:2
    // itr:400, distance:2
    // itr:350, distance:2
    // itr:300, distance:2
    // itr:250, distance:2
    // itr:200, distance:2
    // itr:150, distance:2
    // itr:100, distance:2
    // itr:50, distance:2


//////////////////////////////////////////////////////////////////////////////////////////////

//df target 1
    // itr:0, distance:3731
    // itr:50, distance:3304
    // itr:100, distance:2437
    // itr:150, distance:1587
    // itr:200, distance:1096
    // itr:250, distance:631
    // itr:300, distance:396
    // itr:350, distance:335
    // itr:400, distance:230
    // itr:450, distance:191
    // itr:500, distance:190
    // itr:550, distance:183
    // itr:600, distance:178
    // itr:650, distance:154
    // itr:700, distance:170
    // itr:750, distance:169
    // itr:800, distance:177
    // itr:850, distance:154
    // itr:900, distance:174
    // itr:950, distance:172
    // itr:1000, distance:164
    // itr:1050, distance:174
    // itr:1100, distance:173
    // itr:1150, distance:176
    // itr:1200, distance:171
    // itr:1250, distance:194
    // itr:1300, distance:179
    // itr:1350, distance:176
    // itr:1400, distance:177
    // itr:1450, distance:179
    // itr:1500, distance:184
    // itr:1550, distance:173
    // itr:1600, distance:182
    // itr:1650, distance:166
    // itr:1700, distance:184
    // itr:1750, distance:170
    // itr:1800, distance:187
    // itr:1850, distance:183
    // itr:1900, distance:171
    // itr:1950, distance:159
    // itr:2000, distance:173
    // itr:2050, distance:174
    // itr:2100, distance:175
    // itr:2150, distance:172
    // itr:2200, distance:181
    // itr:2250, distance:181
    // itr:2300, distance:166
    // itr:2350, distance:182
    // itr:2400, distance:162
    // itr:2450, distance:183
    // itr:2500, distance:178
    // itr:2550, distance:172
    // itr:2600, distance:187
    // itr:2650, distance:170
    // itr:2700, distance:178
    // itr:2750, distance:189
    // itr:2800, distance:155
    // itr:2850, distance:177
    // itr:2900, distance:172
    // itr:2950, distance:163
    // itr:3000, distance:172

//df target 2
    // itr:0, distance:3938
    // itr:50, distance:3350
    // itr:100, distance:2441
    // itr:150, distance:1739
    // itr:200, distance:1194
    // itr:250, distance:812
    // itr:300, distance:517
    // itr:350, distance:415
    // itr:400, distance:338
    // itr:450, distance:316
    // itr:500, distance:294
    // itr:550, distance:290
    // itr:600, distance:286
    // itr:650, distance:285
    // itr:700, distance:264
    // itr:750, distance:272
    // itr:800, distance:280
    // itr:850, distance:283
    // itr:900, distance:284
    // itr:950, distance:273
    // itr:1000, distance:292
    // itr:1050, distance:291
    // itr:1100, distance:287
    // itr:1150, distance:284
    // itr:1200, distance:272
    // itr:1250, distance:270
    // itr:1300, distance:269
    // itr:1350, distance:283
    // itr:1400, distance:277
    // itr:1450, distance:285
    // itr:1500, distance:289
    // itr:1550, distance:268
    // itr:1600, distance:286
    // itr:1650, distance:280
    // itr:1700, distance:281
    // itr:1750, distance:287
    // itr:1800, distance:273
    // itr:1850, distance:287
    // itr:1900, distance:279
    // itr:1950, distance:264
    // itr:2000, distance:280
    // itr:2050, distance:283
    // itr:2100, distance:271
    // itr:2150, distance:270
    // itr:2200, distance:279
    // itr:2250, distance:276
    // itr:2300, distance:269
    // itr:2350, distance:275
    // itr:2400, distance:289
    // itr:2450, distance:285
    // itr:2500, distance:272
    // itr:2550, distance:274
    // itr:2600, distance:293
    // itr:2650, distance:283
    // itr:2700, distance:276
    // itr:2750, distance:281
    // itr:2800, distance:283
    // itr:2850, distance:281
    // itr:2900, distance:265
    // itr:2950, distance:288
    // itr:3000, distance:286

//df target 3
    // itr:0, distance:4073
    // itr:50, distance:3427
    // itr:100, distance:2443
    // itr:150, distance:1703
    // itr:200, distance:1286
    // itr:250, distance:1046
    // itr:300, distance:822
    // itr:350, distance:691
    // itr:400, distance:607
    // itr:450, distance:585
    // itr:500, distance:562
    // itr:550, distance:565
    // itr:600, distance:547
    // itr:650, distance:534
    // itr:700, distance:509
    // itr:750, distance:553
    // itr:800, distance:529
    // itr:850, distance:531
    // itr:900, distance:544
    // itr:950, distance:532
    // itr:1000, distance:542
    // itr:1050, distance:536
    // itr:1100, distance:531
    // itr:1150, distance:545
    // itr:1200, distance:522
    // itr:1250, distance:528
    // itr:1300, distance:533
    // itr:1350, distance:550
    // itr:1400, distance:557
    // itr:1450, distance:543
    // itr:1500, distance:549
    // itr:1550, distance:517
    // itr:1600, distance:526
    // itr:1650, distance:538
    // itr:1700, distance:532
    // itr:1750, distance:550
    // itr:1800, distance:536
    // itr:1850, distance:547
    // itr:1900, distance:533
    // itr:1950, distance:534
    // itr:2000, distance:563
    // itr:2050, distance:549
    // itr:2100, distance:546
    // itr:2150, distance:529
    // itr:2200, distance:525
    // itr:2250, distance:549
    // itr:2300, distance:519
    // itr:2350, distance:540
    // itr:2400, distance:538
    // itr:2450, distance:534
    // itr:2500, distance:492
    // itr:2550, distance:534
    // itr:2600, distance:540
    // itr:2650, distance:549
    // itr:2700, distance:535
    // itr:2750, distance:535
    // itr:2800, distance:523
    // itr:2850, distance:555
    // itr:2900, distance:558
    // itr:2950, distance:514
    // itr:3000, distance:522


//df target 4
    // itr:0, distance:4174
    // itr:50, distance:3177
    // itr:100, distance:2299
    // itr:150, distance:1402
    // itr:200, distance:1031
    // itr:250, distance:726
    // itr:300, distance:562
    // itr:350, distance:375
    // itr:400, distance:304
    // itr:450, distance:263
    // itr:500, distance:250
    // itr:550, distance:237
    // itr:600, distance:247
    // itr:650, distance:228
    // itr:700, distance:238
    // itr:750, distance:250
    // itr:800, distance:233
    // itr:850, distance:240
    // itr:900, distance:232
    // itr:950, distance:236
    // itr:1000, distance:236
    // itr:1050, distance:238
    // itr:1100, distance:222
    // itr:1150, distance:230
    // itr:1200, distance:239
    // itr:1250, distance:254
    // itr:1300, distance:241
    // itr:1350, distance:240
    // itr:1400, distance:248
    // itr:1450, distance:249
    // itr:1500, distance:228
    // itr:1550, distance:241
    // itr:1600, distance:243
    // itr:1650, distance:241
    // itr:1700, distance:247
    // itr:1750, distance:247
    // itr:1800, distance:243
    // itr:1850, distance:232
    // itr:1900, distance:241
    // itr:1950, distance:226
    // itr:2000, distance:216
    // itr:2050, distance:228
    // itr:2100, distance:250
    // itr:2150, distance:235
    // itr:2200, distance:240
    // itr:2250, distance:243
    // itr:2300, distance:236
    // itr:2350, distance:243
    // itr:2400, distance:249
    // itr:2450, distance:232
    // itr:2500, distance:228
    // itr:2550, distance:249
    // itr:2600, distance:243
    // itr:2650, distance:244
    // itr:2700, distance:240
    // itr:2750, distance:244
    // itr:2800, distance:237
    // itr:2850, distance:242
    // itr:2900, distance:234
    // itr:2950, distance:231
    // itr:3000, distance:230

//df target 5
    // itr:0, distance:3804
    // itr:50, distance:3261
    // itr:100, distance:2441
    // itr:150, distance:1599
    // itr:200, distance:1129
    // itr:250, distance:743
    // itr:300, distance:491
    // itr:350, distance:401
    // itr:400, distance:309
    // itr:450, distance:255
    // itr:500, distance:263
    // itr:550, distance:246
    // itr:600, distance:255
    // itr:650, distance:231
    // itr:700, distance:242
    // itr:750, distance:248
    // itr:800, distance:254
    // itr:850, distance:243
    // itr:900, distance:253
    // itr:950, distance:249
    // itr:1000, distance:239
    // itr:1050, distance:261
    // itr:1100, distance:241
    // itr:1150, distance:249
    // itr:1200, distance:235
    // itr:1250, distance:259
    // itr:1300, distance:255
    // itr:1350, distance:248
    // itr:1400, distance:260
    // itr:1450, distance:254
    // itr:1500, distance:235
    // itr:1550, distance:257
    // itr:1600, distance:259
    // itr:1650, distance:257
    // itr:1700, distance:249
    // itr:1750, distance:260
    // itr:1800, distance:262
    // itr:1850, distance:252
    // itr:1900, distance:258
    // itr:1950, distance:243
    // itr:2000, distance:250
    // itr:2050, distance:245
    // itr:2100, distance:240
    // itr:2150, distance:253
    // itr:2200, distance:262
    // itr:2250, distance:249
    // itr:2300, distance:254
    // itr:2350, distance:258
    // itr:2400, distance:255
    // itr:2450, distance:268
    // itr:2500, distance:244
    // itr:2550, distance:247
    // itr:2600, distance:263
    // itr:2650, distance:249
    // itr:2700, distance:246
    // itr:2750, distance:265
    // itr:2800, distance:255
    // itr:2850, distance:246
    // itr:2900, distance:254
    // itr:2950, distance:237
    // itr:3000, distance:234