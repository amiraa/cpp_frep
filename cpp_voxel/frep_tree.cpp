#include <iostream>
#include <math.h>
#include "frep.cpp"
#include <vector>

namespace cpp_frep {
    using scalar=float;
    class Frep_tree {
        public:
            bool parametric=false;

            Frep_tree(){
                X=0.0f;
                Y=0.0f;
            };

            Frep_tree(std::vector<std::vector<scalar>>  genome_,bool smooth_,bool parametric_,scalar bound_) {
                X=0.0f;
                Y=0.0f;

                smooth=smooth_;
                parametric=parametric_;
                bound=bound_;

                updateGenome( genome_);
            };

            void updateGenome(std::vector<std::vector<scalar>>  genome_){

                counterAdd          =0;
                counterSubtract     =0;
                counterIntersect    =0;
                counterScale        =0;
                counterMorph        =0;
                counterTranslate    =0;
                counterRotate       =0;
                counterLinearArrayX =0;
                counterLinearArrayY =0;
                counterPolarArray   =0;
                counterCircle       =0;
                counterRectangle    =0;

                genome=genome_;
                constraintGenome();

                addPrimitives();

                if(!smooth){
                    parseTree();
                }else{
                    parseTreeSmooth();
                }
                
            }
            
            std::vector<std::vector<scalar>>  getGenome(){
                auto g= genome;
                for(int i=0; i<numPrimitives;i++){
                    g.pop_back();
                }
                return g;
            }

            Frep_tree get(){
                return *this;
            }

            void setXY(scalar X_,scalar Y_){
                X=X_;
                Y=Y_;
            }

            scalar eval(){
                parsedTree->setXY(X,Y);
                return parsedTree->eval();
            }

            int numNodes(){
                return genome.size()-numPrimitives;//numPrimitives number of primitives
            }

            int dimension(){

                int n=3;
                if(parametric)n=5;

                return numNodes()*n;
            }

            void printGenome(){
                // for (unsigned int i =0; i<genome.size()-numPrimitives;++i){
                //     if(parametric)std::cout<< genome[i][0]<<","<<genome[i][1]<<","<<genome[i][2]<<","<<genome[i][3]<<","<<genome[i][4]<<"  ";
                //     else std::cout<< genome[i][0]<<","<<genome[i][1]<<","<<genome[i][2]<<"  ";
                // }
                std::cout<<getPrintedGenome();
                std::cout<<"\n";

            }

            std::string getPrintedGenome(){
                int n=2;
                if (parametric) n=2;
                std::string g="";
                for (unsigned int i =0; i<genome.size()-numPrimitives;++i){
                    g+=to_string_with_precision(genome[i][0],n);
                    g+="/";
                    g+=to_string_with_precision(genome[i][1],n);
                    g+="/";
                    g+=to_string_with_precision(genome[i][2],n);
                    // if(parametric){
                        g+="/";
                        g+=to_string_with_precision(genome[i][3],n);
                        g+="/";
                        g+=to_string_with_precision(genome[i][4],n);
                    // }
                    g+=" ";
                }
                return g;

            }

            std::string getPrintedFrep(){
                std::string f=""+parsedTree->getPrintedName();
                return f;

            }
            
            Frep* getFrep(){
                return parsedTree;
            }

            virtual ~Frep_tree() {}

        private:
            int maxLength;

            std::vector<std::vector<scalar>>  genome;

            scalar X;
            scalar Y;
            Frep* parsedTree;

            const scalar nodeLength=12.0f;
            static const int numPrimitives=2;
            static const int storageSpace=50;

            Add           ArrayAdd          [storageSpace];
            Subtract      ArraySubtract     [storageSpace];
            Intersect     ArrayIntersect    [storageSpace];
            Scale         ArrayScale        [storageSpace]; 
            Morph         ArrayMorph        [storageSpace*20];
            Translate     ArrayTranslate    [storageSpace];
            Rotate        ArrayRotate       [storageSpace];
            LinearArrayX  ArrayLinearArrayX [storageSpace];
            LinearArrayY  ArrayLinearArrayY [storageSpace];
            PolarArray    ArrayPolarArray   [storageSpace];
            Circle        ArrayCircle       [storageSpace];
            Rectangle     ArrayRectangle    [storageSpace];

            int  counterAdd          =0;
            int  counterSubtract     =0;
            int  counterIntersect    =0;
            int  counterScale        =0;
            int  counterMorph        =0;
            int  counterTranslate    =0;
            int  counterRotate       =0;
            int  counterLinearArrayX =0;
            int  counterLinearArrayY =0;
            int  counterPolarArray   =0;
            int  counterCircle       =0;
            int  counterRectangle    =0;


            bool smooth=false;

            scalar bound=3.0f;

            void addPrimitives(){
                //TODO: PARAMETERS FOR PRIMITIVES
                //TODO: TRIANGLES? OTHER PRIMITIVES
                std::vector<scalar> g,g1;
                g.push_back(10.0f/nodeLength);
                // g.push_back(10.0f/nodeLength);
                g1.push_back(11.0f/nodeLength);
                // g1.push_back(11.0f/nodeLength);
                // g1.push_back(11.0f/nodeLength);
                genome.push_back(g);
                genome.push_back(g1);
                maxLength=genome.size();
            }

            void parseTree(){
                printGenome();
                parsedTree=getNode(0,0);
                std::cout<<"\n";
                parsedTree->printName();
                std::cout<<"\n";
            }

            void parseTreeSmooth(){
                printGenome();
                parsedTree=morphNodeTypes(0);
                std::cout<<"\n";
                parsedTree->printName();
                std::cout<<"\n";
            }

            Frep* morphNodeTypes(int index){

                int nodeIndex1,nodeIndex2;
                nodeIndex1=(int)(genome[index][0]*nodeLength);
                nodeIndex2=nodeIndex1==nodeLength-1?10.0:nodeIndex1+1;
                float weight=genome[index][0]*nodeLength-(float)nodeIndex1;
                

                int n=counterMorph;
                ArrayMorph[n].weight=1.0f-weight;
                std::cout<<"Morph(";
                counterMorph++;
                ArrayMorph[n].setShape1(getNode( index ,nodeIndex1));
                std::cout<<",";
                ArrayMorph[n].setShape2(getNode( index ,nodeIndex2));
                std::cout<<","<<1.0f-weight<<")";
                
                return &ArrayMorph[n];

                
            }

            Frep* morphNodeIndices(int index,float nextIndex){ //num is 1 or 2

                int shapeIndex1,shapeIndex2;
                //TODO CHECK IF RIGHT

                shapeIndex1=index+(int)ceilf(nextIndex* (maxLength-1-index));
                shapeIndex2=(shapeIndex1==(maxLength-1))?(maxLength-1):(shapeIndex1+1);
                float orig=(float)index+ nextIndex* (float)(maxLength-1-index);
                float weight=orig-(float)shapeIndex1+1.0;
                

                int n=counterMorph;
                ArrayMorph[n].weight=weight;
                std::cout<<"Morph(";
                counterMorph++;
                ArrayMorph[n].setShape1(morphNodeTypes(shapeIndex1));
                std::cout<<",";
                ArrayMorph[n].setShape2(morphNodeTypes(shapeIndex2));
                std::cout<<","<<weight<<")";
                
                return &ArrayMorph[n];

                
            }

            Frep* getNode(int index, int nodeIndex){

                if(!smooth) nodeIndex=(int)(genome[index][0]*nodeLength);

                int shape1Index,shape2Index,shapeIndex;
                shape1Index=index+(int)ceilf(genome[index][1]* (maxLength-1-index));
                shape2Index=index+(int)ceilf(genome[index][2]* (maxLength-1-index));
                shapeIndex =index+(int)ceilf(genome[index][1]* (maxLength-1-index));

                //0 add, 1 subtract, 2 intersect, 3 morph, 4 translate, 5 scale, 6 rotate, 7 linearArrayX, 8 linearArrayX, 9 polar array
                switch (nodeIndex)
                {
                    case 0: {
                        // Add f;
                        int n=counterAdd;
                        std::cout<<"Add(";
                        counterAdd++;
                        if (smooth) ArrayAdd[n].setShape1(morphNodeIndices( index,genome[index][1]));
                        else ArrayAdd[n].setShape1(getNode(shape1Index,0));
                        std::cout<<",";
                        if (smooth) ArrayAdd[n].setShape2(morphNodeIndices( index,genome[index][2]));
                        else ArrayAdd[n].setShape2(getNode(shape2Index,0));
                        std::cout<<")";
                        return &ArrayAdd[n];
                    }
                    case 1: {

                        // Subtract f;
                        int n=counterSubtract;
                        std::cout<<"Subtract(";
                        counterSubtract++;
                        if (smooth) ArraySubtract[n].setShape1(morphNodeIndices( index,genome[index][1]));
                        else ArraySubtract[n].setShape1(getNode(shape1Index,0));
                        std::cout<<",";
                        if (smooth) ArraySubtract[n].setShape2(morphNodeIndices( index,genome[index][2]));
                        else ArraySubtract[n].setShape2(getNode(shape2Index,0));
                        std::cout<<")";
                        return &ArraySubtract[n];
                    }
                    case 2: {
                        // Intersect f;
                        int n=counterIntersect;
                        std::cout<<"Intersect(";
                        counterIntersect++;
                        if (smooth) ArrayIntersect[n].setShape1(morphNodeIndices( index,genome[index][1]));
                        else ArrayIntersect[n].setShape1(getNode(shape1Index,0));
                        std::cout<<",";
                        if (smooth) ArrayIntersect[n].setShape2(morphNodeIndices( index,genome[index][2]));
                        else ArrayIntersect[n].setShape2(getNode(shape2Index,0));
                        std::cout<<")";
                        return &ArrayIntersect[n];
                    }
                    case 3: {
                        // Morph f;
                        int n=counterMorph;
                        std::cout<<"Morph(";
                        counterMorph++;
                        if (smooth) ArrayMorph[n].setShape1(morphNodeIndices( index,genome[index][1]));
                        else ArrayMorph[n].setShape1(getNode(shape1Index,0));
                        std::cout<<",";
                        if (smooth) ArrayMorph[n].setShape2(morphNodeIndices( index,genome[index][2]));
                        else ArrayMorph[n].setShape2(getNode(shape2Index,0));
                        std::cout<<")";
                        ArrayMorph[n].weight=0.5;
                        if (parametric) ArrayMorph[n].weight=genome[index][3];
                        else genome[index][3]=ArrayMorph[n].weight;
                        return &ArrayMorph[n];
                    }
                    case 4: {
                        // Translate f;
                        int n=counterTranslate;
                        std::cout<<"Translate(";
                        counterTranslate++;
                        if (smooth) ArrayTranslate[n].setShape(morphNodeIndices( index,genome[index][1]));
                        else ArrayTranslate[n].setShape(getNode(shapeIndex,0));
                        std::cout<<")";
                        if (parametric) {
                            ArrayTranslate[n].dx=genome[index][3]*bound*2.0f-bound;
                            ArrayTranslate[n].dy=genome[index][4]*bound*2.0f-bound;
                        }else{
                            genome[index][3]=(ArrayTranslate[n].dx+bound)/bound/2.0f;
                            genome[index][4]=(ArrayTranslate[n].dy+bound)/bound/2.0f;
                        }
                        return &ArrayTranslate[n];
                    }
                    case 5: {
                        // Scale f;
                        int n=counterScale;
                        std::cout<<"Scale(";
                        counterScale++;
                        if (smooth) ArrayScale[n].setShape(morphNodeIndices( index,genome[index][1]));
                        else ArrayScale[n].setShape(getNode(shapeIndex,0));
                        std::cout<<")";
                        if (parametric) {
                            ArrayScale[n].sx=genome[index][3]*bound*0.75f+0.5f;
                            ArrayScale[n].sy=genome[index][4]*bound*0.75f+0.5f;
                        }else{
                            genome[index][3]=(ArrayScale[n].sx-0.5f)/(bound*0.75);
                            genome[index][4]=(ArrayScale[n].sy-0.5f)/(bound*0.75);

                        }
                        return &ArrayScale[n];
                    }
                    case 6: {
                        //Rotate f;
                        int n=counterRotate;
                        std::cout<<"Rotate(";
                        counterRotate++;
                        if (smooth) ArrayRotate[n].setShape(morphNodeIndices( index,genome[index][1]));
                        else ArrayRotate[n].setShape(getNode(shapeIndex,0));
                        std::cout<<")";
                        if (parametric) ArrayRotate[n].angle=genome[index][3]*360.0;
                        else genome[index][3]=ArrayRotate[n].angle/360.0f;
                        return &ArrayRotate[n];
                    }
                    case 7: {
                        // LinearArrayX f;
                        int n=counterLinearArrayX;
                        std::cout<<"LinearArrayX(";
                        counterLinearArrayX++;
                        if (smooth) ArrayLinearArrayX[n].setShape(morphNodeIndices( index,genome[index][1]));
                        else ArrayLinearArrayX[n].setShape(getNode(shapeIndex,0));
                        std::cout<<")";
                        if (parametric) {
                            ArrayLinearArrayX[n].spacing=genome[index][3]*bound/2.0f;
                            ArrayLinearArrayX[n].number=(int)(genome[index][4]*bound);
                        }else{
                            genome[index][3]=ArrayLinearArrayX[n].spacing*2.0f/bound;
                            genome[index][4]=(scalar)ArrayLinearArrayX[n].number/bound; 
                        }
                        return &ArrayLinearArrayX[n];
                    }
                    case 8: {
                        // LinearArrayY f;
                        int n=counterLinearArrayY;
                        std::cout<<"LinearArrayY(";
                        counterLinearArrayY++;
                        if (smooth) ArrayLinearArrayY[n].setShape(morphNodeIndices( index,genome[index][1]));
                        else ArrayLinearArrayY[n].setShape(getNode(shapeIndex,0));
                        std::cout<<")";
                        if (parametric) {
                            // ArrayLinearArrayY[n].size=genome[index][3]*bound;
                            ArrayLinearArrayY[n].spacing=genome[index][3]*bound/2.0f;
                            ArrayLinearArrayY[n].number=(int)(genome[index][4]*bound);
                        }else{
                            genome[index][3]=ArrayLinearArrayY[n].spacing*2.0f/bound;
                            genome[index][4]=(scalar)ArrayLinearArrayY[n].number/bound; 
                        }
                        return &ArrayLinearArrayY[n];
                    }
                    case 9: {
                        // PolarArray f;
                        int n=counterPolarArray;
                        std::cout<<"PolarArray(";
                        counterPolarArray++;
                        if (smooth) ArrayPolarArray[n].setShape(morphNodeIndices( index,genome[index][1]));
                        else ArrayPolarArray[n].setShape(getNode(shapeIndex,0));
                        std::cout<<")";
                        if (parametric) {
                            ArrayPolarArray[n].angle=genome[index][3]*360.0f;
                            ArrayPolarArray[n].radius=genome[index][4]*bound*0.75f+0.5f;
                        }else{
                            genome[index][3]=ArrayPolarArray[n].angle/360.0;
                            genome[index][4]=(ArrayPolarArray[n].radius-0.5f)/(bound*0.75f); 
                        }
                        return &ArrayPolarArray[n];
                    }
                    case 10: {
                        // Circle f(1.0f,0.0f,0.0f);
                        int n=counterCircle;
                        counterCircle++;
                        std::cout<<"Circle";
                        // if (parametric) ArrayCircle[n].radius=genome[index][3]*bound;
                        return &ArrayCircle[n];
                    }
                    case 11: {
                        // Rectangle f(-1.0f,1.0f,-1.0f,1.0f);
                        int n=counterRectangle;
                        counterRectangle++;
                        std::cout<<"Rectangle";
                        // if (parametric) {
                        //     ArrayTranslate[n].xmin=-genome[index][3]*bound/2.0f;
                        //     ArrayTranslate[n].xmax= genome[index][3]*bound/2.0f;
                        //     ArrayTranslate[n].ymin=-genome[index][4]*bound/2.0f;
                        //     ArrayTranslate[n].ymax= genome[index][4]*bound/2.0f;
                        // }
                        return &ArrayRectangle[n];
                    }
                    default: {
                        int n=counterRectangle;
                        counterRectangle++;
                        std::cout<<"REECCC";
                        // if (parametric) {
                        //     ArrayTranslate[n].xmin=-genome[index][3]*bound/2.0f;
                        //     ArrayTranslate[n].xmax= genome[index][3]*bound/2.0f;
                        //     ArrayTranslate[n].ymin=-genome[index][4]*bound/2.0f;
                        //     ArrayTranslate[n].ymax= genome[index][4]*bound/2.0f;
                        // }
                        return &ArrayRectangle[n];
                    }
                }
            }

            void constraintGenome(){
                for (unsigned int i =0; i<genome.size();++i){
                    genome[i][0]=constraint(genome[i][0]);
                    genome[i][1]=constraint(genome[i][1]);
                    genome[i][2]=constraint(genome[i][2]);
                    if(parametric){
                        genome[i][3]=constraint(genome[i][3]);
                        genome[i][4]=constraint(genome[i][4]);
                    }
                }
                
            }

            scalar constraint(scalar num){
                num=num>0.99999f?0.99999f:num;
                num=num<0.0001f?0.000001f:num;
                return num;
            }

            std::string to_string_with_precision(const scalar a_value, const int n = 2){
                std::ostringstream out;
                out.precision(n);
                out << std::fixed << a_value;
                return out.str();
            }
        
    };
}