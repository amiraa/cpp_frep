#include "frep_tree.cpp"
#include "nelder_mead.h"
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
// #include <nlohmann/json.hpp>



using namespace cpp_frep;
// using json = nlohmann::json;

using scalar=float;
//--------------------------------------------------------------------------------------------------
std::vector<std::string> insert_newlines(const std::string &in, const size_t every_n){
    std::vector<std::string> outStrings;
    int count=0;
    outStrings.push_back("");
    for(std::string::size_type i = 0; i < in.size(); i++) {
        if (!(i % every_n) && i) {
            // out.push_back('\n');
            outStrings.push_back("");
            count++;
        }
        outStrings[count].push_back(in[i]);
    }
    return outStrings;
}
//original for frep in the command line
void drawFrep(Frep* frep,scalar minX,scalar maxX,scalar minY,scalar maxY,scalar dx,scalar dy){
    for(scalar i=minX;i<maxX;i+=dx){
        for(scalar j=minY;j<maxY;j+=dy){
            frep->setXY(i,j);
            scalar val=frep->eval();
            if(val>0.0f){
                std::cout << "+" ;

            }else{
                std::cout << "o" ;
            }
        }
        std::cout  << '\n';
    }
    std::cout  << '\n';

}

//for frep trees in the command line
void drawFrep(Frep_tree frep,scalar minX,scalar maxX,scalar minY,scalar maxY,scalar dx,scalar dy){
    for(scalar i=minX;i<maxX;i+=dx){
        for(scalar j=minY;j<maxY;j+=dy){
            frep.setXY(i,j);
            scalar val=frep.eval();
            if(val>0.0f){
                std::cout << "+" ;

            }else{
                std::cout << "o" ;
            }
        }
        std::cout  << '\n';
    }
    std::cout  << '\n';

}

//for frep trees save and visualize
void drawFrep(Frep_tree frep,scalar minX,scalar maxX,scalar minY,scalar maxY,scalar dx,scalar dy,std::string name,bool preview){

    int height=(int)((maxX-minX)/dx);
    int width=(int)((maxY-minY)/dy);
    const int size=height*width*4;
    


    int stride=0;
    unsigned char pix[size];


    for(scalar i=minX;i<maxX-dx;i+=dx){
        for(scalar j=minY;j<maxY-dy;j+=dy){
            frep.setXY(i,j);
            scalar val=frep.eval();
            if(val>0.0f){
                // std::cout << "+" ;
                pix[stride+0]=0;
                pix[stride+1]=0;
                pix[stride+2]=0;
                pix[stride+3]=255;
                
            }else{
                // std::cout << "o" ;
                pix[stride+0]=255;
                pix[stride+1]=255;
                pix[stride+2]=255;
                pix[stride+3]=0;
            }
            stride+=4;
        }
        // std::cout  << '\n';
    }
    // std::cout  << '\n';
    

    ////////////////create image and save it//////////////////////////
    cv::Mat image = cv::Mat(width, height, CV_8UC4, pix);
    
    float scale=0.3;
    int y=20;
    //print genome
    cv::putText(image, 
        frep.getPrintedGenome(),
        cv::Point(10,y), // Coordinates
        cv::FONT_HERSHEY_SIMPLEX, // Font
        scale, // Scale. 2.0 = 2x bigger
        cv::Scalar(0,0,0), // BGR Color
        1); // Line Thickness (Optional)

    y+=20;

    //print frep
    auto strings=insert_newlines(frep.getPrintedFrep(),120);
    for (unsigned int i=0;i<strings.size();i++){
        cv::putText(image, 
            strings[i],
            cv::Point(10,y), // Coordinates
            cv::FONT_HERSHEY_SIMPLEX, // Font
            scale, // Scale. 2.0 = 2x bigger
            cv::Scalar(0,0,0), // BGR Color
            1); // Line Thickness (Optional)
        y+=15;

    }

    std::string fileName="img/"+name+".png";
    cv::imwrite( fileName, image );
    if(preview){
        cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
        cv::imshow("Display Image", image);
        cv::waitKey(0);

    }
}

//for frep  save and visualize
void drawFrep(Frep* frep,scalar minX,scalar maxX,scalar minY,scalar maxY,scalar dx,scalar dy,std::string name,bool preview){

    int height=(int)((maxX-minX)/dx);
    int width=(int)((maxY-minY)/dy);
    const int size=height*width*4;
    


    int stride=0;
    unsigned char pix[size];


    for(scalar i=minX;i<maxX-dx;i+=dx){
        for(scalar j=minY;j<maxY-dy;j+=dy){
            frep->setXY(i,j);
            scalar val=frep->eval();
            if(val>0.0f){
                // std::cout << "+" ;
                pix[stride+0]=0;
                pix[stride+1]=0;
                pix[stride+2]=0;
                pix[stride+3]=255;
                
            }else{
                // std::cout << "o" ;
                pix[stride+0]=255;
                pix[stride+1]=0;
                pix[stride+2]=0;
                pix[stride+3]=0;
            }
            stride+=4;
        }
        // std::cout  << '\n';
    }
    // std::cout  << '\n';
    
    float scale=0.3;
    int y=20;

    cv::Mat image = cv::Mat(width, height, CV_8UC4, pix);
    cv::putText(image, 
            frep->getPrintedName(),
            cv::Point(10,y), // Coordinates
            cv::FONT_HERSHEY_SIMPLEX, // Font
            scale, // Scale. 2.0 = 2x bigger
            cv::Scalar(0,0,0), // BGR Color
            1); // Line Thickness (Optional)

    std::string fileName="img/"+name+".png";
    cv::imwrite( fileName, image );
    if(preview){
        cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
        cv::imshow("Display Image", image);
        cv::waitKey(0);

    }
}

//compare between a target frep and target frep tree
float compareL1(Vector v, Frep* frep,Frep_tree* frep_tree,scalar bound, scalar res ,std::string name, bool tuneParameters){

    scalar minX=-bound;
    scalar maxX=bound;
    scalar minY=-bound;
    scalar maxY=bound;
    scalar dx=res;
    scalar dy=res;

    int height=(int)((maxX-minX)/dx);
    int width=(int)((maxY-minY)/dy);
    const int size=height*width*4;
    
    std::string fileName="img/"+name+".png";
    
    int stride=0;
    unsigned char pix[size];
    int same=0;


    std::vector<std::vector<scalar>>  g= frep_tree->getGenome();
    if (frep_tree->parametric){
        if(tuneParameters){
            for (int i =0; i<frep_tree->numNodes();++i){
                g[i][3]=(v.at(i*2+0));
                g[i][4]=(v.at(i*2+1));
            }

        }else{
            for (int i =0; i<frep_tree->numNodes();++i){
                g[i][0]=(v.at(i*5+0));
                g[i][1]=(v.at(i*5+1));
                g[i][2]=(v.at(i*5+2));
                g[i][3]=(v.at(i*5+3));
                g[i][4]=(v.at(i*5+4));
            }
        }
        
    }else{
        for (int i =0; i<frep_tree->numNodes();++i){
            g[i][0]=(v.at(i*3+0));
            g[i][1]=(v.at(i*3+1));
            g[i][2]=(v.at(i*3+2));
        }
    }

    
    
    frep_tree->updateGenome(g);

    Circle c(1.0f,0.0f,0.0f);
    Circle c1(0.25f,0.0f,0.0f);
    Scale sc(frep_tree->getFrep(),0.0f,0.0f,0.25f,0.25f);
    PolarArray pa2(&sc,45.0f,0.75f);
    Add add(&pa2,&c);
    Subtract newGear (&add,&c1);


    for(scalar i=minX;i<maxX-dx;i+=dx){
        for(scalar j=minY;j<maxY-dy;j+=dy){
            newGear.setXY(i,j);
            scalar val_tree=newGear.eval();


            frep->setXY(i,j);
            scalar val=frep->eval();

            if((val>0.0&&val_tree>0.0) || (val<0.0&&val_tree<0.0)){
                same++;
            }
            
            if(val_tree>0.0f){
                // std::cout << "+" ;
                pix[stride+0]=0;
                pix[stride+1]=0;
                pix[stride+2]=0;
                pix[stride+3]=0;
                
            }else{
                // std::cout << "o" ;
                pix[stride+0]=255;
                pix[stride+1]=255;
                pix[stride+2]=255;
                pix[stride+3]=255;
            }
            stride+=4;
        }
        // std::cout  << '\n';
    }
    // std::cout  << '\n';
    float result=  (float)same/((float)size/4.0f); 
    std::cout<<result <<"\n";   
    

    ////////////////create image and save it//////////////////////////

    cv::Mat image = cv::Mat(width, height, CV_8UC4, pix);

    float scale=0.3;
    int y=20;

    //print genome
    cv::putText(image, 
        frep_tree->getPrintedGenome(),
        cv::Point(10,y), // Coordinates
        cv::FONT_HERSHEY_SIMPLEX, // Font
        scale, // Scale. 2.0 = 2x bigger
        cv::Scalar(0,0,0), // BGR Color
        1); // Line Thickness (Optional)

    y+=20;

    //print frep
    auto strings=insert_newlines(frep_tree->getPrintedFrep(),120);
    for (unsigned int i=0;i<strings.size();i++){
        cv::putText(image, 
            strings[i],
            cv::Point(10,y), // Coordinates
            cv::FONT_HERSHEY_SIMPLEX, // Font
            scale, // Scale. 2.0 = 2x bigger
            cv::Scalar(0,0,0), // BGR Color
            1); // Line Thickness (Optional)
        y+=15;

    }
    
    cv::imwrite( fileName, image );
    return result;

}

//get random float between 0 and 1
scalar getRandom(){//between 0 and 1
    return ((float) rand() / (RAND_MAX));
}
scalar getNoise(scalar scale){//between 0 and 1
    return ((scalar) rand() / (RAND_MAX))/scale-scale/2.0;
}

//get random genome
Vector getRandomGenome(int n){
    std::vector<float> v;
    for(int i=0;i<n;++i){
        v.push_back(getRandom());
    }
    return Vector(v);
}

Vector getParameters(std::vector<std::vector<scalar>> genome,bool noise, scalar scale){
    std::vector<scalar> v;
    for (unsigned int i =0; i<genome.size();++i){
        if (noise){
            v.push_back(genome[i][3]+getNoise(scale));
            v.push_back(genome[i][4]+getNoise(scale));   

        }else{
            v.push_back(genome[i][3]);
            v.push_back(genome[i][4]);
        }
    }
    return Vector(v);
}

void nelderMead(Frep_tree* tree,Frep* target,float precision, int maxSteps, bool tuneParameters, scalar bound, scalar res){

    scalar minX=-bound;
    scalar maxX=bound;
    scalar minY=-bound;
    scalar maxY=bound;
    scalar dx=res;
    scalar dy=res;

    drawFrep(target,minX, maxX, minY, maxY, dx, dy,"target",false);


    int dimension=tree->dimension();
    if (tuneParameters) dimension= tree->numNodes()*2;
    NelderMeadOptimizer o(dimension, precision);

    // request a simplex to start with

    Vector v;


    if(tuneParameters){
        
        v=getParameters(tree->getGenome(),false, 0.001f);
        o.insert(v);
        o.insert(getParameters(tree->getGenome(),false, 0.001f));
        o.insert(getParameters(tree->getGenome(),false, 0.001f));
        std::cout<<"dim: "<<dimension<<", "<<v.dimension()<<"\n";
    }else{
        v=getRandomGenome(dimension);
        o.insert(v);
        o.insert(getRandomGenome(dimension));
        o.insert(getRandomGenome(dimension));

    }
    
    

    int count=0;
    int count1=0;
    float score=0.0f;

    // while ( !o.done(score)&&count<maxSteps) { //cap 
    while ( !o.done(score)) {
        // if(count>maxSteps||o.done()){
        if(count>maxSteps){
            count=0;
            std::cout<<"Counld't find solution, reset with random values!!!!!!!!!!!!!!!!!!!\n";
            o.reset();
            if(!tuneParameters){
                o.insert(v);
                o.insert(getRandomGenome(dimension));
                o.insert(getRandomGenome(dimension));

            }else{
                o.insert(getParameters(tree->getGenome(),false, 0.001f));
                o.insert(getParameters(tree->getGenome(),false, 0.001f));

            }
            
        }
        std::cout<<"Step:"<<count<<", total:"<< count1<<"\n";

        std::string name1 = "i";
        if(tuneParameters) name1 = "j";
        std::string name=name1+std::to_string(count);   
        score=compareL1( v, target, tree, bound,res , name,tuneParameters);
        v = o.step(v,score );
        count++;
        count1++;
    }
}


int main(int const, char const**) {
   
    scalar res=0.01f;
    scalar bound =3.0f;


    bool smooth=true;
    bool parametric=false;
    bool tuneParameters=false;

    float precision = 0.001f;

    
    scalar minX=-bound;
    scalar maxX=bound;
    scalar minY=-bound;
    scalar maxY=bound;
    scalar dx=res;
    scalar dy=res;


    // std::vector<std::vector<scalar>>  genome;


    // std::vector<scalar> node1;
    // node1.push_back(0.564);
    // node1.push_back(0.53);
    // node1.push_back(0.445);
    // node1.push_back(0.5024);
    // node1.push_back(0.7475);
   
    // genome.push_back(node1);

    // std::vector<scalar> node2;
    // node2.push_back(0.48);
    // node2.push_back(0.3);
    // node2.push_back(0.9);
    // node2.push_back(0.17);
    // node2.push_back(0.227);
    
    // genome.push_back(node2);

    // std::vector<scalar> node3;
    // node3.push_back(0.2472);
    // node3.push_back(0.452);
    // node3.push_back(0.727);
    // node3.push_back(0.17);
    // node3.push_back(0.227);
    // genome.push_back(node3);

    // Frep_tree tree(genome,smooth,parametric,bound); 
    
    // // Circle c(0.5f,0.0f,0.0f);
    // // Rectangle r(-0.3f,0.3f,-0.3f,0.3f);
    // // Subtract s(&r,&c);
    // // PolarArray pa(&r,45.0f,1.0f);

    


    // // PolarArray pa2(&r,45.0f,0.5f);
    // // InvoluteGear gear( 1.0f,  1.0f, 1.1f, 20.0f, 8.0f) ;
    // // Scale sc(&gear,0.0f,0.0f,0.25f,0.25f);
    // // drawFrep(&sc,minX, maxX, minY, maxY, dx, dy,"gear",true);

    // Circle c(1.0f,0.0f,0.0f);
    // Circle c1(0.25f,0.0f,0.0f);
    // // Scale sc(tree.getFrep(),0.0f,0.0f,0.25f,0.25f);
    // // PolarArray pa2(&sc,45.0f,0.75f);
    // // Add add(&pa2,&c);
    // // Subtract sub (&add,&c1);
    // // drawFrep(&sub,minX, maxX, minY, maxY, dx, dy,"scaleAndRotated",true);

    // InvoluteGear gear( 1.0f,  1.0f, 1.1f, 20.0f, 8.0f) ;
    // Rotate rot (&gear,0.0f,0.0f,15.0f);
    // Scale sc1(&rot,0.0f,0.0f,0.3f,0.3f);
    // Add add1(&sc1,&c);
    // Subtract target (&add1,&c1);
    // drawFrep(&target,minX, maxX, minY, maxY, dx, dy,"target",false);


    // int maxSteps=100;
    // precision=0.009;
    // // nelderMead(&tree,&target, precision, maxSteps,  tuneParameters,  bound,  res);

    // drawFrep(tree,minX, maxX, minY, maxY, dx, dy,"final",false);

    

    // best 5 nodes 0.00103913
    std::vector<scalar> params5={0.56645, 1.13557, 0.601257, 0.225314, 0.579243, 0.534814, 0.530563, 0.64434, 0.208775, 0.0954164, 0.309497, 0.632866, 0.341624, 0.124673, 0.376787, 0.542705, 0.481462, 0.369699, 0.751949, 0.717642, 0.326457, 0.211514, 0.34966, 0.284553, 0.438133, 2.60259};
    params5={0.2,
                    1.15608,
                    0.532703,
                    0.236274,
                    0.631038,
                    0.520627,
                    0.551214,
                    0.682205,
                    0.243995,
                    0.1164,
                    0.34611,
                    0.620567,
                    0.331086,
                    0.132638,
                    0.462726,
                    0.550451,
                    0.490349,
                    0.348071,
                    0.759609,
                    0.740685,
                    0.341778,
                    0.223557,
                    0.290785,
                    0.308138,
                    0.453482,
                    2.60747};

    //BEST //0.0007
    std::vector<scalar> params2={0.611069,0.353383,0.427557,0.764276,0.724136,0.339219,0.743547,0.309012,0.803405,0.0938337,2.60517};

    unsigned n_nodes_=5;
    std::vector<std::vector<scalar>>  genome;
    std::vector<std::vector<scalar>> nodes;
    for (unsigned i=0;i<n_nodes_;i++ ){
        std::vector<scalar> node;
        for (unsigned j=0;j<5;j++ ){
            node.push_back(params5[i*5+j]);
        }
        nodes.push_back(node);
        genome.push_back(nodes[i]);
    }
    Frep_tree tree(genome,smooth,parametric,bound); 

    Add gear ( 
                new Circle(1.0 ,0.0,0.0),//body
                new PolarArray  ( //teeth
                    new Scale ( //scale
                        tree.getFrep() //tooth //get teeth
                    ,0.0f,0.0f, 0.28f,0.28f) //scale
                , 360.0/8.0,0.75) 
    ); 
    
    drawFrep(&gear,minX, maxX, minY, maxY, dx, dy,"best_"+std::to_string(n_nodes_),true); 
    
    std::cout<< gear.getPrintedName()<<'\n';
   

    // //////////tune parameters//////////
    // std::cout<<"Tuning parameters!!!!!!!!!!!!!!!!!!!\n";
    // parametric=true;
    // tree.parametric=parametric;
    // tuneParameters=true;
    // precision=0.0075;
    // maxSteps=20;

    // nelderMead(&tree,&pa, precision, maxSteps,  tuneParameters,  bound,  res);

    // drawFrep(tree,minX, maxX, minY, maxY, dx, dy,"finalt",false);

    //json 
    // unsigned n_nodes_=2;
    // std::ifstream i2("2_nodes.json");
    // std::ifstream i5("5_nodes.json");
    // scalar scale_factor=1.5;
    // int n_teeth_=8;
    // json results;
    // if(n_nodes_==2){
    //     i2 >> results;
    // }else{
    //     i5 >> results;
    // }
    

    // std::cout<<results["population"]["0"][0]["point"]<<'\n';

    // std::cout<<results["Best Index"].size()<<'\n';
    // unsigned numGenerations=results["Best Index"].size();
    // unsigned numPopulation=results["population"]["0"].size();
    
    // //show best each iteration
    // for (unsigned generation=0;generation<numGenerations;generation++ ){
    //     std::vector<std::vector<scalar>>  genome;
    //     std::vector<std::vector<scalar>> nodes;
    //     std::cout<<std::to_string(generation)<<'\n';
    //     unsigned bestPointIndex= results["Best Index"][std::to_string(generation)];
    //     std::cout<<bestPointIndex<<'\n';
    //     auto bestGenome=results["population"][std::to_string(generation)][bestPointIndex]["point"];
    //     std::cout<<bestGenome<<'\n';
    //     for (unsigned i=0;i<n_nodes_;i++ ){
    //         std::vector<scalar> node;
    //         for (unsigned j=0;j<5;j++ ){
    //             node.push_back(bestGenome[i*5+j]);
    //         }
    //         nodes.push_back(node);
    //         genome.push_back(nodes[i]);
    //         Frep_tree tree(genome,smooth,parametric,bound); 
    //         Scale gear ( //scale
    //                 new Add ( //body and teeth
    //                     new Circle(1.0 ,0.0,0.0),//body
    //                     new PolarArray  ( //teeth
    //                         new Scale ( //scale
    //                             tree.getFrep() //tooth //get teeth
    //                         ,0.0f,0.0f, 0.28f,0.28f) //scale
    //                 , 360.0/(scalar)n_teeth_,0.75) ) //body and teeth
    //             ,scale_factor);//scale
            
    //         drawFrep(&gear,minX, maxX, minY, maxY, dx, dy,"node"+std::to_string(n_nodes_)+"/"+std::to_string(generation),false); 
    //         //std::to_string()
    //     }
    // }

    // //save first 10 generations
    // for (unsigned generation=0;generation<5;generation++ ){
    //     for (unsigned population=0;population<numPopulation;population++ ){
    //         std::vector<std::vector<scalar>>  genome;
    //         std::vector<std::vector<scalar>> nodes;
    //         auto bestGenome=results["population"][std::to_string(generation)][population]["point"];
    //         for (unsigned i=0;i<n_nodes_;i++ ){
    //             std::vector<scalar> node;
    //             for (unsigned j=0;j<5;j++ ){
    //                 node.push_back(bestGenome[i*5+j]);
    //             }
    //             nodes.push_back(node);
    //             genome.push_back(nodes[i]);
    //             Frep_tree tree(genome,smooth,parametric,bound); 
    //             Scale gear ( //scale
    //                     new Add ( //body and teeth
    //                         new Circle(1.0 ,0.0,0.0),//body
    //                         new PolarArray  ( //teeth
    //                             new Scale ( //scale
    //                                 tree.getFrep() //tooth //get teeth
    //                             ,0.0f,0.0f, 0.28f,0.28f) //scale
    //                     , 360.0/(scalar)n_teeth_,0.75) ) //body and teeth
    //                 ,scale_factor);//scale
                
    //             drawFrep(&gear,minX, maxX, minY, maxY, dx, dy,"first"+std::to_string(n_nodes_)+"/"+std::to_string(generation)+"_"+std::to_string(population),false); 
    //             //std::to_string()
    //         }
    //     }
    // }

    // // write prettified JSON to another file
    // std::ofstream o("pretty_5_nodes.json");
    // o << std::setw(4) << results_2_nodes << std::endl;
    
    //0 add, 1 subtract, 2 intersect, 3 morph, 4 translate, 5 scale, 6 rotate, 7 linearArray, 8 polar array

    return 0;
}


