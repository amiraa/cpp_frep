# cpp_frep

A simple c++ frep library to design custom functional representation of geometries.

## Dependencies

Similar to [cpp_starter](https://gitlab.cba.mit.edu/erik/cpp_starter/tree/master):
- a modern C++ compiler (i.e. one that supports C++17 features, ex. gcc 7+)
- cmake 3.13 or newer
- Eigen3 (installable via apt, yum, or homebrew)
- openCV (installable via apt, yum, or homebrew)
- json Library: https://github.com/nlohmann/json

## Building

From the project's root directory,

```
mkdir build
cd build
cmake ..
make
```

Tips:
- you can build using multiple threads at once using e.g. `make -j4`
- you can specify a specific compiler using `cmake .. -DCMAKE_CXX_COMPILER=g++-8`
- for debugging symbols use `cmake .. -DCMAKE_BUILD_TYPE=Debug`

## Running

From the build directory,

```
cpp_frep/cpp_frep
```
